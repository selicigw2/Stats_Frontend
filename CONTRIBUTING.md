# Contributing


## Process
 1. Fork the project
 2. Work on your modification
 3. Create Merge request back in
 
## Running the project
This is a Create-react-App based project so the basics from that apply.  
``yarn`` or ``npm install`` to install the base components.  
``yarn start`` or ``npm start`` to bring up the delevopment site on localhost:3000.  

## Notes
* If you have any queries on wheather it will be accepted or not plaease contact me ahead of time.