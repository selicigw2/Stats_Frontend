import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router} from "react-router-dom";
import Routes from "./routes";
import Header from './navbar'

// Css for everything, importing once
import './_css/highChartsDark.css'
import './_css/bootstrap_slate.css'
import './_css/index.css';
import './_css/App.css';

const App = () => (
  <Router>
    <div className='App container'>
      <Header />
      <Routes />
    </div>
  </Router>)

ReactDOM.render(<App />, document.getElementById('root'));