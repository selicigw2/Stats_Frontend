export const getRarityColour = (rarity: string) => {
  let rarityColour
  switch(rarity){
    case"Junk":{
      rarityColour = "#AAA"
      break
    }
    case"Basic":{
      rarityColour = "#ffffff"
      break
    }
    case"Fine":{
      rarityColour = "#62A4DA"
      break
    }
    case"Masterwork":{
      rarityColour = "#1a9306"
      break
    }
    case"Rare":{
      rarityColour = "#fcd00b"
      break
    }
    case"Exotic":{
      rarityColour = "#ffa405"
      break
    }
    case"Ascended":{
      rarityColour = "#fb3e8d"
      break
    }
    case"Legendary":{
      rarityColour = "#9370DB"
      break
    }
    default:{
      rarityColour = "#AAA"
    }
  }
  return rarityColour
}

export const getDarthsCDN = (url: string) => {
  // using Darth's CDN
  let imgRegex = /https:\/\/render\.guildwars2\.com\/file\/(.*?)\.png/
  let imgDarth = 'https://darthmaim-cdn.de/gw2treasures/icons/$1.png'
  if (typeof url !== 'undefined') {
    return url.replace(imgRegex, imgDarth)
  } else {
    return ''
  }
}

export const convertToGold = (number:number, shorten?:boolean) => {
  let input = Math.floor(number).toString()
  let minus = ''
  if (input.includes('-')) {
    minus = '-'
  }
  input = input.replace('-', '')

  let copper = input.slice(-2) + 'c'
  let silver = input.slice(-4, -2) + 's'
  let gold = input.slice(0, -4) + 'g'

  if (copper.length === 1) {
    copper = ''
  }
  if (silver.length === 1) {
    silver = ''
  }
  if (gold.length === 1) {
    gold = ''
  }

  let result = minus + gold + silver + copper
  if (shorten) {
    if (number >= 100) {
      // remove trailing copper
      result = result.replace('00c', '')
    }

    if (number >= 10000) {
      // remove trailing silver
      result = result.replace('00s', '')
    }
  }

  return result
}

function stringToNumber (a:string|number|undefined):number {
  if (typeof a === 'undefined') {return 0}
  return parseInt(a.toString().replace(/[^\d.-]/g, ''), 10) || 0
}

export const sortGeneral =(a:string|number|undefined, b:string|number|undefined) =>{
  a = stringToNumber(a)
  b = stringToNumber(b)
  return a > b ? 1 : -1
}

export const  filterGeneral =(item:object, filter:string, multiplier =1 ) =>{
  let start = 0
  let end = Infinity

  let split = filter.split(',')
  if (split.length > 0) { start = parseInt(split[0],10) * multiplier }
  if (split.length > 1) { end = parseInt(split[1],10) * multiplier }

  let converted = parseInt(item.toString().replace(/[^\d.-]/g, ''),10) || 0
  return converted >= start && converted <= end
}

export const capitalize = ([first,...rest]: string) => first.toUpperCase() + rest.join('').toLowerCase()