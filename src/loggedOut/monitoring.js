import React, { Component } from 'react'
import custom from "silveress_custom"

import Highcharts from 'highcharts/highstock'
import {
  HighchartsStockChart,
  Chart,
  withHighcharts,
  XAxis,
  YAxis,
  Legend,
  Title,
  PlotBand,
  SplineSeries,
  Navigator,
  Tooltip,
  RangeSelector
} from 'react-jsx-highstock'
import { graphOptions, urlBase } from '../_utiilities/data'
import { Loading} from '../_utiilities/functions_react'

class SiteStatsViewer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: []
    }
  }

  async loadData (url, extra) { return window.fetch(url, extra).then(response => response.json()).then(data => { return data }).catch((err) => { console.log(this.props.url, err.toString()); return [] }) }

  async componentDidMount () {
    let tmp = {}
    tmp.data = await this.loadData(urlBase.parser + '/v1/siteStats?beautify=min', { method: 'GET', headers: { 'Content-Type': 'application/json', session: this.state.session } })
    this.setState(tmp)
  }

  static processGraph (data) {
    let processedData = {}
    data = custom.sorting(data, 'date',false)
    let tmp = {}
    for (let i = 0; i < data.length; i++) {
      let date = new Date(data[i].date).getTime()
      let itemsRaw = Object.keys(data[i])
      for (let j = 0; j < itemsRaw.length; j++) { if (typeof tmp[itemsRaw[j]] === 'undefined') { tmp[itemsRaw[j]] = '' } }
      let items = Object.keys(tmp)
      for (let j = 0; j < items.length; j++) {
        if (items[j] === 'date') { continue }
        let name = items[j]
        if(items[j] === "POST /gw2/v1/website/account"){name = "Account"}
        if(items[j] === "total"){name = " Total"}
        if(items[j].indexOf("controller_") !== -1){name = custom.capitalize(items[j].replace("controller_",""))}
        if(typeof processedData[name] === "undefined"){
          processedData[name] = []
        }
        if (typeof data[i][items[j]] !== 'undefined') {
          processedData[name].push([date, Math.floor(data[i][items[j]])])
        } else {
          processedData[name].push([date, 0])
        }
      }
    }
    return processedData
  }
  static renderPlotBand (band, index) {
    const { from, to } = band
    const id = `${from}-${to}`
    const color = (index % 2) ? '#ff362e' : 'rgba(255, 55, 48, 0.4)'
    return (
      <PlotBand key={id} from={from} to={to} color={color}>
        <PlotBand.Label>{band.label}</PlotBand.Label>
      </PlotBand>
    )
  }
  render () {
    let loading
    let graphOut,graphIn
    if (this.state.data.length === 0) {
      loading = <Loading />
    } else {
      let graphData, categories, series, navigator
      graphData = SiteStatsViewer.processGraph(this.state.data.out)
      categories = Object.keys(graphData)
      categories.sort()
      series = []
      navigator = []
      for (let i = 0; i < categories.length; i++) {
        let currentSeries = <SplineSeries
          name={categories[i]}
          id={categories[i]}
          key={i + 'series' + categories[i]}
          data={graphData[categories[i]]}
          dataGrouping={{ enabled: true, approximation: 'high' }}
          visible
          animationLimit={0}
        />
        series.push(currentSeries)
        navigator.push(<Navigator.Series seriesId={categories[i]} key={i + 'nav' + categories[i]}/>)
      }
      const bands = [
        { label: 'limit', from: 600, to: 700 }
      ]
      graphOut = <div>
        <HighchartsStockChart
          styledMode
          plotOptions={graphOptions.plotOptions}
        >
          <Chart/>
          <Title>Requests made to official Guildwars2 API</Title>
          <Legend/>

          <RangeSelector
            //selected={8}
          >
            <RangeSelector.Button count={1} type='hour'>1h</RangeSelector.Button>
            <RangeSelector.Button count={12} type='hour'>12h</RangeSelector.Button>
            <RangeSelector.Button count={1} type='day'>1d</RangeSelector.Button>
            <RangeSelector.Button count={7} type='day'>7d</RangeSelector.Button>
            <RangeSelector.Button count={1} type='month'>1m</RangeSelector.Button>
            <RangeSelector.Button count={3} type='month'>3m</RangeSelector.Button>
            <RangeSelector.Button count={12} type='month'>1y</RangeSelector.Button>
            <RangeSelector.Button type='all'>All</RangeSelector.Button>
            <RangeSelector.Input boxBorderColor='#7cb5ec'/>
          </RangeSelector>

          <Tooltip
            shared
            formatter={function () {
              let s = '<b> ' + new Date(this.x).toISOString() + '</b>'
              for (let i = 0; i < this.points.length; i++) {
                s += '<br/><span style="color:' + this.points[i].color +
                  '">\u25CF</span>' + this.points[i].series.name + ': ' +
                  this.points[i].y.toLocaleString('en-IE',
                    { maximumFractionDigits: 0 })
              }
              return s
            }}
          />

          <XAxis/>
          <YAxis>
            {series}
            {bands.map(SiteStatsViewer.renderPlotBand)}
          </YAxis>

          <Navigator>
            {navigator}
          </Navigator>
        </HighchartsStockChart>
      </div>

      graphData = SiteStatsViewer.processGraph(this.state.data.in)
      categories = Object.keys(graphData)
      categories.sort()
      series = []
      navigator = []
      for (let i = 0; i < categories.length; i++) {
        let currentSeries = <SplineSeries
          name={categories[i]}
          id={categories[i]}
          key={i + 'series' + categories[i]}
          data={graphData[categories[i]]}
          dataGrouping={{ enabled: true, approximation: 'high' }}
          visible
          animationLimit={0}
        />
        series.push(currentSeries)
        navigator.push(<Navigator.Series seriesId={categories[i]} key={i + 'nav' + categories[i]}/>)
      }

      graphIn = <div>
        <HighchartsStockChart
          styledMode
          plotOptions={graphOptions.plotOptions}
        >
          <Chart/>
          <Title>Requests made to Silver's API</Title>
          <Legend/>

          <RangeSelector
            //selected={8}
          >
            <RangeSelector.Button count={1} type='hour'>1h</RangeSelector.Button>
            <RangeSelector.Button count={12} type='hour'>12h</RangeSelector.Button>
            <RangeSelector.Button count={1} type='day'>1d</RangeSelector.Button>
            <RangeSelector.Button count={7} type='day'>7d</RangeSelector.Button>
            <RangeSelector.Button count={1} type='month'>1m</RangeSelector.Button>
            <RangeSelector.Button count={3} type='month'>3m</RangeSelector.Button>
            <RangeSelector.Button count={12} type='month'>1y</RangeSelector.Button>
            <RangeSelector.Button type='all'>All</RangeSelector.Button>
            <RangeSelector.Input boxBorderColor='#7cb5ec'/>
          </RangeSelector>

          <Tooltip
            shared
            formatter={function () {
              let s = '<b> ' + new Date(this.x).toISOString() + '</b>'
              for (let i = 0; i < this.points.length; i++) {
                s += '<br/><span style="color:' + this.points[i].color +
                  '">\u25CF</span>' + this.points[i].series.name + ': ' +
                  this.points[i].y.toLocaleString('en-IE',
                    { maximumFractionDigits: 0 })
              }
              return s
            }}
          />

          <XAxis/>

          <YAxis>
            {series}
          </YAxis>

          <Navigator>
            {navigator}
          </Navigator>
        </HighchartsStockChart>
      </div>
    }

    return <div style={{align:"center"}}>
      {loading}
      {graphOut}
      {graphIn}
    </div>
  }
}

export default withHighcharts(SiteStatsViewer, Highcharts)