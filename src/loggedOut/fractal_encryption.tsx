import React, {Component} from 'react'
import {urlBase} from '../_utiilities/data.json'
import {Documentation, GetCurrency, Loading} from '../_utiilities/functions_react'
import {capitalize} from '../_utiilities/functions'


interface Objects {
    [propName: string]: any;
}


interface Fractal_Main_State {
    itemData?: Objects[]
    processed: Processed
}

interface Processed {
    totals?: Processed_Sub
    totalsAverage?: Processed_Sub
    values?: Processed_Sub
    valuesAverage?: Processed_Sub
    valuesTaxAverage?: Processed_Sub
}
interface Processed_Sub extends Object {
    total: number
    relics: number
    keys: number
    gear:number
    mew:number
    materials: Processed_Sub_object
    aetherized: Processed_Sub_object
    junk: Processed_Sub_object
    recipe: Processed_Sub_object
    [propName: string]: any
}
interface Processed_Sub_object {
    total: number
    [propName: string]: number
}

// main function for managing the subtools
export class Fractal_Main extends Component<any, Fractal_Main_State> {
    constructor(props: any) {
        super(props)
        this.state = {
            itemData: undefined,
            processed: {
                totals: undefined,
                totalsAverage: undefined,
                values: undefined,
                valuesAverage: undefined,
                valuesTaxAverage: undefined,
            }
        }
    }

    // load up base data, will be used down further
    async componentDidMount() {
        let url = `${urlBase.parser}/v1/items/json?fields=id,name,sell_price,buy_price&beautify=min`
        let itemData = await fetch(url).then(response => response.json()).catch(err => {console.error(url, err.toString()); return []})
        this.setState({ itemData: itemData, processed: this.processData(itemData) })
    }

    // process teh data, will be used int eh subcomponents
    processData = (itemData: Objects[]): Processed =>{
        let totals: Processed_Sub = {
            // this is the number of boxes
            total: 0,
            relics: 0,
            keys: 0,
            gear:0,
            mew:0,
            // this collapses down, uses total when collapsed
            aetherized: { total: 0 },
            junk: { total: 0 },
            materials: { total: 0 },
            recipe: { total: 0 }
        }
        let totalsAverage: Processed_Sub = {
            total: 0,
            relics: 0,
            keys: 0,
            gear:0,
            mew:0,
            // this collapses down, uses total when collapsed
            aetherized: { total: 0 },
            junk: { total: 0 },
            materials: { total: 0 },
            recipe: { total: 0 }
        }
        let values: Processed_Sub = {
            total: 0,
            relics: 0,
            keys: 0,
            gear:0,
            mew:0,
            // this collapses down, uses total when collapsed
            aetherized: { total: 0 },
            junk: { total: 0 },
            materials: { total: 0 },
            recipe: { total: 0 }
        }
        let valuesAverage: Processed_Sub = {
            total: 0,
            relics: 0,
            keys: 0,
            gear:0,
            mew:0,
            // this collapses down, uses total when collapsed
            aetherized: { total: 0 },
            junk: { total: 0 },
            materials: { total: 0 },
            recipe: { total: 0 }
        }
        let valuesTax: Processed_Sub = {
            total: 0,
            relics: 0,
            keys: 0,
            gear:0,
            mew:0,
            // this collapses down, uses total when collapsed
            aetherized: { total: 0 },
            junk: { total: 0 },
            materials: { total: 0 },
            recipe: { total: 0 }
        }
        let valuesTaxAverage: Processed_Sub = {
            total: 0,
            relics: 0,
            keys: 0,
            gear:0,
            mew:0,
            // this collapses down, uses total when collapsed
            aetherized: { total: 0 },
            junk: { total: 0 },
            materials: { total: 0 },
            recipe: { total: 0 }
        }

        const aetherisedItems = itemData.filter(item => item.name.indexOf("Aetherized") !== -1).filter(item => item.name.indexOf("Skin") !== -1)
        for(let i=0;i<initialFractalEncryptionData.length;i++){
            for (let [key, value] of Object.entries(initialFractalEncryptionData[i])) {
                let valueParsed = parseInt(value, 10) ||  0

                // handle anything with a 1:1 relationship
                if(typeof totals[key] !== "undefined"){
                    totals[key] += valueParsed
                    totalsAverage[key] = totals[key]/totals.total

                    // Mini Professor Mew
                    if(key === "mew"){
                        let mew = itemData.filter(item => item.name === "Mini Professor Mew")
                        let valueItem = 0
                        if(mew.length !== 0){
                            valueItem = mew[0].sell_price
                        }
                        valueItem = valueItem * valueParsed

                        values.mew += valueItem
                        valuesAverage.mew = values.mew/totals.total

                        values.total += valueItem

                        valuesTax.mew += valueItem * 0.85
                        valuesTaxAverage.mew = valuesTax.mew/totals.total

                        values.total += valueItem
                        valuesTax.total += valueItem * 0.85
                    }

                    continue
                }

                // handle aetherized
                if(key.indexOf("Aetherized") !== -1){
                    let stripped = key.replace("Aetherized", "").replace("Skin", "").trim()

                    if(typeof totals.aetherized[stripped] === "undefined"){
                        totals.aetherized[stripped] = 0
                        values.aetherized[stripped] = 0
                        valuesTax.aetherized[stripped] = 0
                    }

                    totals.aetherized.total += valueParsed
                    totals.aetherized[stripped] += valueParsed

                    totalsAverage.aetherized.total = totals.aetherized.total/totals.total
                    totalsAverage.aetherized[stripped] = totals.aetherized[stripped]/totals.total

                    // values

                    let valueRaw = aetherisedItems.filter(item => item.name.indexOf(stripped) !== -1)

                    let valueItem = 0
                    if(valueRaw.length !== 0){
                        valueItem = valueRaw[0].sell_price
                    }
                    valueItem = valueItem * valueParsed
                    values.aetherized.total += valueItem
                    values.aetherized[stripped] += valueItem
                    valuesAverage.aetherized.total = values.aetherized.total/totals.total
                    valuesAverage.aetherized[stripped] = values.aetherized[stripped]/totals.total

                    values.total += valueItem


                    valuesTax.aetherized.total += valueItem * 0.85
                    valuesTax.aetherized[stripped] += valueItem * 0.85
                    valuesTaxAverage.aetherized.total = valuesTax.aetherized.total/totals.total
                    valuesTaxAverage.aetherized[stripped] = valuesTax.aetherized[stripped]/totals.total

                    valuesTax.total += valueItem * 0.85
                    continue
                }

                // handle recipes
                if(key.indexOf("Recipe") !== -1){
                    let stripped = key.replace("Recipe:", "").trim()

                    if(typeof totals.recipe[stripped] === "undefined"){
                        totals.recipe[stripped] = 0
                    }
                    totals.recipe.total += valueParsed
                    totals.recipe[stripped] += valueParsed

                    totalsAverage.recipe.total = totals.recipe.total/totals.total
                    totalsAverage.recipe[stripped] = totals.recipe[stripped]/totals.total

                    // values are 0, no need to do calcs

                    continue
                }
                // gold items
                let junk_items = ["Manuscripts", "Proofs", "Treatises", "Postulates"]
                if(junk_items.indexOf(key) !== -1){
                    let stripped = key

                    if(typeof totals.junk[stripped] === "undefined"){
                        totals.junk[stripped] = 0
                        values.junk[stripped] = 0
                        valuesTax.junk[stripped] = 0
                    }
                    totals.junk.total += valueParsed
                    totals.junk[stripped] += valueParsed

                    totalsAverage.junk.total = totals.junk.total/totals.total
                    totalsAverage.junk[stripped] = totals.junk[stripped]/totals.total


                    // value
                    let valueItem = 0
                    switch (key){
                        case "Manuscripts":{
                            valueItem = 6000
                            break
                        }
                        case "Proofs":{
                            valueItem = 3000
                            break
                        }
                        case "Treatises":{
                            valueItem = 2500
                            break
                        }
                        case "Postulates":{
                            valueItem = 2000
                            break
                        }
                    }
                    valueItem = valueItem * valueParsed
                    values.junk.total += valueItem
                    values.junk[stripped] += valueItem
                    valuesAverage.junk.total = values.junk.total/totals.total
                    valuesAverage.junk[stripped] = values.junk[stripped]/totals.total

                    values.total += valueItem


                    valuesTax.junk.total += valueItem
                    valuesTax.junk[stripped] += valueItem
                    valuesTaxAverage.junk.total = valuesTax.junk.total/totals.total
                    valuesTaxAverage.junk[stripped] = valuesTax.junk[stripped]/totals.total

                    valuesTax.total += valueItem
                    continue
                }



                // remainder are materials
                let stripped = key.trim()
                if(typeof totals.materials[stripped] === "undefined"){
                    totals.materials[stripped] = 0
                    values.materials[stripped] = 0
                    valuesTax.materials[stripped] = 0
                }
                totals.materials.total += valueParsed
                totals.materials[stripped] += valueParsed

                totalsAverage.materials.total = totals.materials.total/totals.total
                totalsAverage.materials[stripped] = totals.materials[stripped]/totals.total

                let material = itemData.filter(item => item.name === stripped)
                let valueItem = 0
                if(material.length !== 0){
                    valueItem = material[0].sell_price
                }
                valueItem = valueItem * valueParsed
                values.materials.total += valueItem
                values.materials[stripped] += valueItem
                valuesAverage.materials.total = values.materials.total/totals.total
                valuesAverage.materials[stripped] = values.materials[stripped]/totals.total

                values.total += valueItem


                valuesTax.materials.total += valueItem
                valuesTax.materials[stripped] += valueItem
                valuesTaxAverage.materials.total = valuesTax.materials.total/totals.total
                valuesTaxAverage.materials[stripped] = valuesTax.materials[stripped]/totals.total

                valuesTax.total += valueItem
            }
        }


        valuesAverage.total = values.total/totals.total
        valuesTaxAverage.total = valuesTax.total/totals.total

        return {
            totals,
            totalsAverage,
            values,
            valuesAverage,
            valuesTaxAverage
        }
    }

    render() {
        let {itemData, processed} = this.state
        if(!itemData){return <Loading />}

        return <div
            // style={{"align":"center"}}
        >
            <Documentation url={"https://gitlab.com/Silvers_Gw2/Stats_Frontend/-/wikis/misc#alt-standard"} />
            <br/>
            <div>
                <h3>Fractal Encryption's</h3>
                <h5>Based on opening {processed.totals?.total} containers</h5>
            </div>
            <br />
            <FractalOutputs processed={processed}/>
            <br />
            <FractalCost processed={processed} itemData={itemData} />
        </div>
    }
}

// each tool has its own class

// total = boxes
let initialFractalEncryptionData = [
    { "total": "10000", "relics": "663", "keys": "973", "Vial of Potent Blood": "3400", "Large Bone": "3415", "Large Claw": "3470", "Large Scale": "3530", "Large Fang": "3440", "Intricate Totem": "3305", "Potent Venom Sac": "3340", "Pile of Incandescent Dust": "3365", "+1 Agony Infusion": "22600", "Dragonite Ore": "9565", "Empyreal Fragment": "10570", "Pile of Bloodstone Dust": "10195", "gear": "1", "mew": "201", "Manuscripts": "2810", "Proofs": "4294", "Treatises": "2914", "Postulates": "2796", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "2", "Aetherized Rifle Skin": "1", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "1", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "1", "Recipe: Dagger": "1", "Recipe: Focus": "3", "Recipe: Greatsword": "2", "Recipe: Hammer": "", "Recipe: Longbow": "1", "Recipe: Mace": "", "Recipe: Pistol": "3", "Recipe: Rifle": "1", "Recipe: Scepter": "4", "Recipe: Shield": "1", "Recipe: Shortbow": "1", "Recipe: Staff": "1", "Recipe: Sword": "5", "Recipe: Torch": "2", "Recipe: Warhorn": "2", "Recipe: Trident": "2", "Recipe: Harpoon Gun": "", "Recipe: Spear": "", "Recipe: Light Coat": "2", "Recipe: Light Leggings": "4", "Recipe: Light Gloves": "6", "Recipe: Light Boots": "2", "Recipe: Light Helm": "", "Recipe: Light Shoulders": "5", "Recipe: Medium Coat": "1", "Recipe: Medium Leggings": "2", "Recipe: Medium Gloves": "1", "Recipe: Medium Boots": "4", "Recipe: Medium Helm": "3", "Recipe: Medium Shoulders": "1", "Recipe: Heavy Coat": "2", "Recipe: Heavy Leggings": "3", "Recipe: Heavy Gloves": "1", "Recipe: Heavy Boots": "1", "Recipe: Heavy Helm": "4", "Recipe: Heavy Shoulders": "3" },
    { "total": "10000", "relics": "683", "keys": "981", "Vial of Potent Blood": "3245", "Large Bone": "3290", "Large Claw": "3230", "Large Scale": "3300", "Large Fang": "3105", "Intricate Totem": "3505", "Potent Venom Sac": "3585", "Pile of Incandescent Dust": "3385", "+1 Agony Infusion": "22685", "Dragonite Ore": "10135", "Empyreal Fragment": "9825", "Pile of Bloodstone Dust": "10735", "gear": "3", "mew": "200", "Manuscripts": "2848", "Proofs": "4299", "Treatises": "2817", "Postulates": "2867", "Aetherized Axe Skin": "1", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "1", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "1", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "1", "Recipe: Dagger": "1", "Recipe: Focus": "4", "Recipe: Greatsword": "2", "Recipe: Hammer": "", "Recipe: Longbow": "1", "Recipe: Mace": "1", "Recipe: Pistol": "2", "Recipe: Rifle": "2", "Recipe: Scepter": "5", "Recipe: Shield": "1", "Recipe: Shortbow": "2", "Recipe: Staff": "", "Recipe: Sword": "", "Recipe: Torch": "3", "Recipe: Warhorn": "2", "Recipe: Trident": "1", "Recipe: Harpoon Gun": "4", "Recipe: Spear": "2", "Recipe: Light Coat": "2", "Recipe: Light Leggings": "1", "Recipe: Light Gloves": "2", "Recipe: Light Boots": "2", "Recipe: Light Helm": "3", "Recipe: Light Shoulders": "2", "Recipe: Medium Coat": "3", "Recipe: Medium Leggings": "1", "Recipe: Medium Gloves": "5", "Recipe: Medium Boots": "3", "Recipe: Medium Helm": "1", "Recipe: Medium Shoulders": "1", "Recipe: Heavy Coat": "4", "Recipe: Heavy Leggings": "6", "Recipe: Heavy Gloves": "1", "Recipe: Heavy Boots": "1", "Recipe: Heavy Helm": "1", "Recipe: Heavy Shoulders": "2" },
    { "total": "15000", "relics": "1039", "keys": "1448", "Vial of Potent Blood": "5350", "Large Bone": "5160", "Large Claw": "4770", "Large Scale": "5085", "Large Fang": "5315", "Intricate Totem": "5025", "Potent Venom Sac": "5105", "Pile of Incandescent Dust": "4915", "+1 Agony Infusion": "34022", "Dragonite Ore": "14250", "Empyreal Fragment": "15360", "Pile of Bloodstone Dust": "15420", "gear": "2", "mew": "303", "Manuscripts": "3932", "Proofs": "6593", "Treatises": "4356", "Postulates": "4305", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "1", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "1", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "2", "Recipe: Dagger": "7", "Recipe: Focus": "2", "Recipe: Greatsword": "3", "Recipe: Hammer": "4", "Recipe: Longbow": "5", "Recipe: Mace": "1", "Recipe: Pistol": "3", "Recipe: Rifle": "2", "Recipe: Scepter": "6", "Recipe: Shield": "2", "Recipe: Shortbow": "2", "Recipe: Staff": "2", "Recipe: Sword": "3", "Recipe: Torch": "3", "Recipe: Warhorn": "5", "Recipe: Trident": "3", "Recipe: Harpoon Gun": "3", "Recipe: Spear": "1", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "6", "Recipe: Light Gloves": "4", "Recipe: Light Boots": "6", "Recipe: Light Helm": "3", "Recipe: Light Shoulders": "4", "Recipe: Medium Coat": "2", "Recipe: Medium Leggings": "3", "Recipe: Medium Gloves": "4", "Recipe: Medium Boots": "6", "Recipe: Medium Helm": "2", "Recipe: Medium Shoulders": "", "Recipe: Heavy Coat": "", "Recipe: Heavy Leggings": "2", "Recipe: Heavy Gloves": "9", "Recipe: Heavy Boots": "3", "Recipe: Heavy Helm": "2", "Recipe: Heavy Shoulders": "5" },
    { "total": "20000", "relics": "1358", "keys": "2001", "Vial of Potent Blood": "6840", "Large Bone": "6740", "Large Claw": "7090", "Large Scale": "6955", "Large Fang": "6540", "Intricate Totem": "6770", "Potent Venom Sac": "6315", "Pile of Incandescent Dust": "6635", "+1 Agony Infusion": "45383", "Dragonite Ore": "20735", "Empyreal Fragment": "19545", "Pile of Bloodstone Dust": "20785", "gear": "5", "mew": "405", "Manuscripts": "5980", "Proofs": "8379", "Treatises": "5728", "Postulates": "5669", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "1", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "1", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "1", "Aetherized Spear Skin": "", "Recipe: Axe": "8", "Recipe: Dagger": "5", "Recipe: Focus": "5", "Recipe: Greatsword": "4", "Recipe: Hammer": "8", "Recipe: Longbow": "7", "Recipe: Mace": "2", "Recipe: Pistol": "4", "Recipe: Rifle": "6", "Recipe: Scepter": "2", "Recipe: Shield": "6", "Recipe: Shortbow": "4", "Recipe: Staff": "7", "Recipe: Sword": "3", "Recipe: Torch": "3", "Recipe: Warhorn": "2", "Recipe: Trident": "1", "Recipe: Harpoon Gun": "5", "Recipe: Spear": "4", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "2", "Recipe: Light Gloves": "3", "Recipe: Light Boots": "5", "Recipe: Light Helm": "4", "Recipe: Light Shoulders": "2", "Recipe: Medium Coat": "5", "Recipe: Medium Leggings": "4", "Recipe: Medium Gloves": "3", "Recipe: Medium Boots": "6", "Recipe: Medium Helm": "2", "Recipe: Medium Shoulders": "1", "Recipe: Heavy Coat": "1", "Recipe: Heavy Leggings": "2", "Recipe: Heavy Gloves": "9", "Recipe: Heavy Boots": "10", "Recipe: Heavy Helm": "6", "Recipe: Heavy Shoulders": "4" },
    { "total": "20000", "relics": "1318", "keys": "1985", "Vial of Potent Blood": "6650", "Large Bone": "6505", "Large Claw": "6705", "Large Scale": "6885", "Large Fang": "7020", "Intricate Totem": "6805", "Potent Venom Sac": "6955", "Pile of Incandescent Dust": "6770", "+1 Agony Infusion": "45292", "Dragonite Ore": "20530", "Empyreal Fragment": "20295", "Pile of Bloodstone Dust": "20300", "gear": "2", "mew": "393", "Manuscripts": "5620", "Proofs": "8466", "Treatises": "5756", "Postulates": "5789", "Aetherized Axe Skin": "1", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "1", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "1", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "1", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "2", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "5", "Recipe: Dagger": "4", "Recipe: Focus": "6", "Recipe: Greatsword": "3", "Recipe: Hammer": "4", "Recipe: Longbow": "6", "Recipe: Mace": "4", "Recipe: Pistol": "7", "Recipe: Rifle": "8", "Recipe: Scepter": "4", "Recipe: Shield": "6", "Recipe: Shortbow": "7", "Recipe: Staff": "4", "Recipe: Sword": "6", "Recipe: Torch": "4", "Recipe: Warhorn": "4", "Recipe: Trident": "5", "Recipe: Harpoon Gun": "4", "Recipe: Spear": "4", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "2", "Recipe: Light Gloves": "4", "Recipe: Light Boots": "5", "Recipe: Light Helm": "2", "Recipe: Light Shoulders": "3", "Recipe: Medium Coat": "5", "Recipe: Medium Leggings": "8", "Recipe: Medium Gloves": "2", "Recipe: Medium Boots": "1", "Recipe: Medium Helm": "3", "Recipe: Medium Shoulders": "2", "Recipe: Heavy Coat": "5", "Recipe: Heavy Leggings": "7", "Recipe: Heavy Gloves": "4", "Recipe: Heavy Boots": "4", "Recipe: Heavy Helm": "1", "Recipe: Heavy Shoulders": "4" },
    { "total": "20000", "relics": "1260", "keys": "1985", "Vial of Potent Blood": "7095", "Large Bone": "6500", "Large Claw": "6640", "Large Scale": "6810", "Large Fang": "6640", "Intricate Totem": "6915", "Potent Venom Sac": "6555", "Pile of Incandescent Dust": "6720", "+1 Agony Infusion": "45298", "Dragonite Ore": "19785", "Empyreal Fragment": "21900", "Pile of Bloodstone Dust": "19735", "gear": "6", "mew": "371", "Manuscripts": "5630", "Proofs": "8596", "Treatises": "5679", "Postulates": "5759", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "1", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "1", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "2", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "1", "Recipe: Axe": "2", "Recipe: Dagger": "2", "Recipe: Focus": "7", "Recipe: Greatsword": "3", "Recipe: Hammer": "5", "Recipe: Longbow": "3", "Recipe: Mace": "13", "Recipe: Pistol": "2", "Recipe: Rifle": "7", "Recipe: Scepter": "2", "Recipe: Shield": "2", "Recipe: Shortbow": "7", "Recipe: Staff": "4", "Recipe: Sword": "4", "Recipe: Torch": "3", "Recipe: Warhorn": "2", "Recipe: Trident": "4", "Recipe: Harpoon Gun": "7", "Recipe: Spear": "6", "Recipe: Light Coat": "1", "Recipe: Light Leggings": "3", "Recipe: Light Gloves": "6", "Recipe: Light Boots": "3", "Recipe: Light Helm": "5", "Recipe: Light Shoulders": "6", "Recipe: Medium Coat": "3", "Recipe: Medium Leggings": "4", "Recipe: Medium Gloves": "2", "Recipe: Medium Boots": "7", "Recipe: Medium Helm": "3", "Recipe: Medium Shoulders": "6", "Recipe: Heavy Coat": "8", "Recipe: Heavy Leggings": "5", "Recipe: Heavy Gloves": "3", "Recipe: Heavy Boots": "3", "Recipe: Heavy Helm": "3", "Recipe: Heavy Shoulders": "3" },
    { "total": "20000", "relics": "1334", "keys": "1989", "Vial of Potent Blood": "6530", "Large Bone": "6980", "Large Claw": "6290", "Large Scale": "6840", "Large Fang": "7015", "Intricate Totem": "6580", "Potent Venom Sac": "6840", "Pile of Incandescent Dust": "6660", "+1 Agony Infusion": "45445", "Dragonite Ore": "19825", "Empyreal Fragment": "20785", "Pile of Bloodstone Dust": "19455", "gear": "7", "mew": "410", "Manuscripts": "5618", "Proofs": "8572", "Treatises": "5761", "Postulates": "5730", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "1", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "1", "Recipe: Axe": "4", "Recipe: Dagger": "6", "Recipe: Focus": "4", "Recipe: Greatsword": "3", "Recipe: Hammer": "5", "Recipe: Longbow": "4", "Recipe: Mace": "3", "Recipe: Pistol": "7", "Recipe: Rifle": "7", "Recipe: Scepter": "1", "Recipe: Shield": "5", "Recipe: Shortbow": "1", "Recipe: Staff": "6", "Recipe: Sword": "6", "Recipe: Torch": "2", "Recipe: Warhorn": "9", "Recipe: Trident": "4", "Recipe: Harpoon Gun": "4", "Recipe: Spear": "4", "Recipe: Light Coat": "5", "Recipe: Light Leggings": "6", "Recipe: Light Gloves": "6", "Recipe: Light Boots": "", "Recipe: Light Helm": "5", "Recipe: Light Shoulders": "8", "Recipe: Medium Coat": "6", "Recipe: Medium Leggings": "5", "Recipe: Medium Gloves": "3", "Recipe: Medium Boots": "4", "Recipe: Medium Helm": "2", "Recipe: Medium Shoulders": "6", "Recipe: Heavy Coat": "1", "Recipe: Heavy Leggings": "6", "Recipe: Heavy Gloves": "6", "Recipe: Heavy Boots": "5", "Recipe: Heavy Helm": "2", "Recipe: Heavy Shoulders": "2" },
    { "total": "10000", "relics": "613", "keys": "1019", "Vial of Potent Blood": "3415", "Large Bone": "3225", "Large Claw": "3295", "Large Scale": "3455", "Large Fang": "3515", "Intricate Totem": "3625", "Potent Venom Sac": "3330", "Pile of Incandescent Dust": "3395", "+1 Agony Infusion": "22663", "Dragonite Ore": "10230", "Empyreal Fragment": "10550", "Pile of Bloodstone Dust": "9415", "gear": "0", "mew": "195", "Manuscripts": "2866", "Proofs": "4378", "Treatises": "2841", "Postulates": "2818", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "2", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "6", "Recipe: Dagger": "1", "Recipe: Focus": "3", "Recipe: Greatsword": "2", "Recipe: Hammer": "1", "Recipe: Longbow": "", "Recipe: Mace": "3", "Recipe: Pistol": "3", "Recipe: Rifle": "2", "Recipe: Scepter": "1", "Recipe: Shield": "1", "Recipe: Shortbow": "2", "Recipe: Staff": "2", "Recipe: Sword": "2", "Recipe: Torch": "1", "Recipe: Warhorn": "5", "Recipe: Trident": "1", "Recipe: Harpoon Gun": "2", "Recipe: Spear": "", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "4", "Recipe: Light Gloves": "2", "Recipe: Light Boots": "2", "Recipe: Light Helm": "1", "Recipe: Light Shoulders": "", "Recipe: Medium Coat": "1", "Recipe: Medium Leggings": "3", "Recipe: Medium Gloves": "1", "Recipe: Medium Boots": "2", "Recipe: Medium Helm": "", "Recipe: Medium Shoulders": "1", "Recipe: Heavy Coat": "1", "Recipe: Heavy Leggings": "1", "Recipe: Heavy Gloves": "3", "Recipe: Heavy Boots": "", "Recipe: Heavy Helm": "4", "Recipe: Heavy Shoulders": "" },
    { "total": "20000", "relics": "1371", "keys": "2028", "Vial of Potent Blood": "6850", "Large Bone": "6980", "Large Claw": "6695", "Large Scale": "6680", "Large Fang": "6470", "Intricate Totem": "6795", "Potent Venom Sac": "6900", "Pile of Incandescent Dust": "7085", "+1 Agony Infusion": "45113", "Dragonite Ore": "20610", "Empyreal Fragment": "20255", "Pile of Bloodstone Dust": "19440", "gear": "5", "mew": "411", "Manuscripts": "5786", "Proofs": "8574", "Treatises": "5697", "Postulates": "5735", "Aetherized Axe Skin": "1", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "1", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "1", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "1", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "1", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "1", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "2", "Aetherized Spear Skin": "1", "Recipe: Axe": "3", "Recipe: Dagger": "5", "Recipe: Focus": "6", "Recipe: Greatsword": "4", "Recipe: Hammer": "5", "Recipe: Longbow": "1", "Recipe: Mace": "3", "Recipe: Pistol": "4", "Recipe: Rifle": "3", "Recipe: Scepter": "4", "Recipe: Shield": "2", "Recipe: Shortbow": "3", "Recipe: Staff": "4", "Recipe: Sword": "3", "Recipe: Torch": "4", "Recipe: Warhorn": "", "Recipe: Trident": "4", "Recipe: Harpoon Gun": "2", "Recipe: Spear": "7", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "2", "Recipe: Light Gloves": "5", "Recipe: Light Boots": "3", "Recipe: Light Helm": "5", "Recipe: Light Shoulders": "2", "Recipe: Medium Coat": "3", "Recipe: Medium Leggings": "3", "Recipe: Medium Gloves": "6", "Recipe: Medium Boots": "2", "Recipe: Medium Helm": "1", "Recipe: Medium Shoulders": "3", "Recipe: Heavy Coat": "3", "Recipe: Heavy Leggings": "3", "Recipe: Heavy Gloves": "2", "Recipe: Heavy Boots": "4", "Recipe: Heavy Helm": "2", "Recipe: Heavy Shoulders": "" },
    { "total": "11000", "relics": "712", "keys": "1087", "Vial of Potent Blood": "3775", "Large Bone": "3560", "Large Claw": "4105", "Large Scale": "3825", "Large Fang": "3485", "Intricate Totem": "3855", "Potent Venom Sac": "3830", "Pile of Incandescent Dust": "3815", "+1 Agony Infusion": "24864", "Dragonite Ore": "11015", "Empyreal Fragment": "11120", "Pile of Bloodstone Dust": "11580", "gear": "3", "mew": "210", "Manuscripts": "3130", "Proofs": "4689", "Treatises": "3124", "Postulates": "3168", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "1", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "1", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "1", "Recipe: Dagger": "1", "Recipe: Focus": "1", "Recipe: Greatsword": "2", "Recipe: Hammer": "3", "Recipe: Longbow": "", "Recipe: Mace": "3", "Recipe: Pistol": "1", "Recipe: Rifle": "3", "Recipe: Scepter": "4", "Recipe: Shield": "3", "Recipe: Shortbow": "4", "Recipe: Staff": "1", "Recipe: Sword": "2", "Recipe: Torch": "3", "Recipe: Warhorn": "5", "Recipe: Trident": "2", "Recipe: Harpoon Gun": "4", "Recipe: Spear": "2", "Recipe: Light Coat": "2", "Recipe: Light Leggings": "1", "Recipe: Light Gloves": "", "Recipe: Light Boots": "2", "Recipe: Light Helm": "2", "Recipe: Light Shoulders": "3", "Recipe: Medium Coat": "2", "Recipe: Medium Leggings": "3", "Recipe: Medium Gloves": "4", "Recipe: Medium Boots": "", "Recipe: Medium Helm": "1", "Recipe: Medium Shoulders": "1", "Recipe: Heavy Coat": "1", "Recipe: Heavy Leggings": "3", "Recipe: Heavy Gloves": "1", "Recipe: Heavy Boots": "2", "Recipe: Heavy Helm": "2", "Recipe: Heavy Shoulders": "2" },
    { "total": "13000", "relics": "876", "keys": "1283", "Vial of Potent Blood": "4715", "Large Bone": "4385", "Large Claw": "4305", "Large Scale": "4375", "Large Fang": "4535", "Intricate Totem": "4405", "Potent Venom Sac": "4325", "Pile of Incandescent Dust": "4235", "+1 Agony Infusion": "29412", "Dragonite Ore": "13510", "Empyreal Fragment": "12980", "Pile of Bloodstone Dust": "13000", "gear": "2", "mew": "256", "Manuscripts": "3660", "Proofs": "5445", "Treatises": "3752", "Postulates": "3777", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "1", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "2", "Recipe: Dagger": "3", "Recipe: Focus": "3", "Recipe: Greatsword": "1", "Recipe: Hammer": "3", "Recipe: Longbow": "1", "Recipe: Mace": "1", "Recipe: Pistol": "2", "Recipe: Rifle": "1", "Recipe: Scepter": "3", "Recipe: Shield": "1", "Recipe: Shortbow": "4", "Recipe: Staff": "3", "Recipe: Sword": "2", "Recipe: Torch": "2", "Recipe: Warhorn": "2", "Recipe: Trident": "1", "Recipe: Harpoon Gun": "3", "Recipe: Spear": "1", "Recipe: Light Coat": "", "Recipe: Light Leggings": "5", "Recipe: Light Gloves": "5", "Recipe: Light Boots": "4", "Recipe: Light Helm": "2", "Recipe: Light Shoulders": "4", "Recipe: Medium Coat": "4", "Recipe: Medium Leggings": "1", "Recipe: Medium Gloves": "1", "Recipe: Medium Boots": "5", "Recipe: Medium Helm": "4", "Recipe: Medium Shoulders": "1", "Recipe: Heavy Coat": "4", "Recipe: Heavy Leggings": "2", "Recipe: Heavy Gloves": "2", "Recipe: Heavy Boots": "4", "Recipe: Heavy Helm": "2", "Recipe: Heavy Shoulders": "2" },
    { "total": "24000", "relics": "1618", "keys": "2446", "Vial of Potent Blood": "8090", "Large Bone": "7970", "Large Claw": "8320", "Large Scale": "7785", "Large Fang": "8120", "Intricate Totem": "7610", "Potent Venom Sac": "8155", "Pile of Incandescent Dust": "7990", "+1 Agony Infusion": "54694", "Dragonite Ore": "24845", "Empyreal Fragment": "24580", "Pile of Bloodstone Dust": "23930", "gear": "3", "mew": "483", "Manuscripts": "6762", "Proofs": "10317", "Treatises": "6968", "Postulates": "6812", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "1", "Aetherized Greatsword Skin": "1", "Aetherized Hammer Skin": "1", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "1", "Aetherized Staff Skin": "3", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "2", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "7", "Recipe: Dagger": "6", "Recipe: Focus": "4", "Recipe: Greatsword": "4", "Recipe: Hammer": "4", "Recipe: Longbow": "4", "Recipe: Mace": "10", "Recipe: Pistol": "2", "Recipe: Rifle": "4", "Recipe: Scepter": "6", "Recipe: Shield": "9", "Recipe: Shortbow": "7", "Recipe: Staff": "3", "Recipe: Sword": "3", "Recipe: Torch": "1", "Recipe: Warhorn": "6", "Recipe: Trident": "4", "Recipe: Harpoon Gun": "11", "Recipe: Spear": "5", "Recipe: Light Coat": "5", "Recipe: Light Leggings": "3", "Recipe: Light Gloves": "", "Recipe: Light Boots": "6", "Recipe: Light Helm": "5", "Recipe: Light Shoulders": "9", "Recipe: Medium Coat": "4", "Recipe: Medium Leggings": "5", "Recipe: Medium Gloves": "3", "Recipe: Medium Boots": "5", "Recipe: Medium Helm": "3", "Recipe: Medium Shoulders": "6", "Recipe: Heavy Coat": "3", "Recipe: Heavy Leggings": "4", "Recipe: Heavy Gloves": "5", "Recipe: Heavy Boots": "5", "Recipe: Heavy Helm": "2", "Recipe: Heavy Shoulders": "4" },
    { "total": "24000", "relics": "1550", "keys": "2459", "Vial of Potent Blood": "7915", "Large Bone": "8250", "Large Claw": "8365", "Large Scale": "8370", "Large Fang": "7620", "Intricate Totem": "8305", "Potent Venom Sac": "8190", "Pile of Incandescent Dust": "8250", "+1 Agony Infusion": "54383", "Dragonite Ore": "23630", "Empyreal Fragment": "25055", "Pile of Bloodstone Dust": "24690", "gear": "1", "mew": "507", "Manuscripts": "6832", "Proofs": "10198", "Treatises": "6949", "Postulates": "6842", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "", "Aetherized Focus Skin": "1", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "1", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "2", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "1", "Recipe: Axe": "3", "Recipe: Dagger": "2", "Recipe: Focus": "5", "Recipe: Greatsword": "5", "Recipe: Hammer": "9", "Recipe: Longbow": "3", "Recipe: Mace": "2", "Recipe: Pistol": "5", "Recipe: Rifle": "6", "Recipe: Scepter": "2", "Recipe: Shield": "4", "Recipe: Shortbow": "13", "Recipe: Staff": "7", "Recipe: Sword": "7", "Recipe: Torch": "4", "Recipe: Warhorn": "3", "Recipe: Trident": "6", "Recipe: Harpoon Gun": "4", "Recipe: Spear": "5", "Recipe: Light Coat": "11", "Recipe: Light Leggings": "3", "Recipe: Light Gloves": "4", "Recipe: Light Boots": "8", "Recipe: Light Helm": "7", "Recipe: Light Shoulders": "6", "Recipe: Medium Coat": "4", "Recipe: Medium Leggings": "7", "Recipe: Medium Gloves": "5", "Recipe: Medium Boots": "3", "Recipe: Medium Helm": "3", "Recipe: Medium Shoulders": "6", "Recipe: Heavy Coat": "4", "Recipe: Heavy Leggings": "3", "Recipe: Heavy Gloves": "8", "Recipe: Heavy Boots": "4", "Recipe: Heavy Helm": "5", "Recipe: Heavy Shoulders": "3" },
    { "total": "20000", "relics": "1358", "keys": "1993", "Vial of Potent Blood": "7110", "Large Bone": "6690", "Large Claw": "6990", "Large Scale": "6905", "Large Fang": "6940", "Intricate Totem": "6720", "Potent Venom Sac": "6335", "Pile of Incandescent Dust": "6715", "+1 Agony Infusion": "45197", "Dragonite Ore": "19285", "Empyreal Fragment": "20590", "Pile of Bloodstone Dust": "20495", "gear": "3", "mew": "349", "Manuscripts": "5770", "Proofs": "8494", "Treatises": "5720", "Postulates": "5700", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "1", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "1", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "1", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "1", "Recipe: Axe": "1", "Recipe: Dagger": "4", "Recipe: Focus": "2", "Recipe: Greatsword": "5", "Recipe: Hammer": "5", "Recipe: Longbow": "5", "Recipe: Mace": "7", "Recipe: Pistol": "5", "Recipe: Rifle": "3", "Recipe: Scepter": "4", "Recipe: Shield": "4", "Recipe: Shortbow": "2", "Recipe: Staff": "5", "Recipe: Sword": "2", "Recipe: Torch": "8", "Recipe: Warhorn": "6", "Recipe: Trident": "6", "Recipe: Harpoon Gun": "2", "Recipe: Spear": "3", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "1", "Recipe: Light Gloves": "8", "Recipe: Light Boots": "6", "Recipe: Light Helm": "5", "Recipe: Light Shoulders": "5", "Recipe: Medium Coat": "8", "Recipe: Medium Leggings": "6", "Recipe: Medium Gloves": "7", "Recipe: Medium Boots": "6", "Recipe: Medium Helm": "5", "Recipe: Medium Shoulders": "2", "Recipe: Heavy Coat": "5", "Recipe: Heavy Leggings": "5", "Recipe: Heavy Gloves": "8", "Recipe: Heavy Boots": "1", "Recipe: Heavy Helm": "4", "Recipe: Heavy Shoulders": "4" },
    { "total": "50000", "relics": "3418", "keys": "5031", "Vial of Potent Blood": "16880", "Large Bone": "16375", "Large Claw": "16815", "Large Scale": "17310", "Large Fang": "17410", "Intricate Totem": "16915", "Potent Venom Sac": "16770", "Pile of Incandescent Dust": "17075", "+1 Agony Infusion": "113244", "Dragonite Ore": "51285", "Empyreal Fragment": "50645", "Pile of Bloodstone Dust": "50110", "gear": "5", "mew": "970", "Manuscripts": "14180", "Proofs": "21208", "Treatises": "14412", "Postulates": "14315", "Aetherized Axe Skin": "1", "Aetherized Dagger Skin": "1", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "1", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "2", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "1", "Aetherized Staff Skin": "1", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "1", "Aetherized Warhorn Skin": "1", "Aetherized Trident Skin": "1", "Aetherized Harpoon Gun Skin": "2", "Aetherized Spear Skin": "", "Recipe: Axe": "14", "Recipe: Dagger": "11", "Recipe: Focus": "5", "Recipe: Greatsword": "10", "Recipe: Hammer": "9", "Recipe: Longbow": "17", "Recipe: Mace": "15", "Recipe: Pistol": "8", "Recipe: Rifle": "9", "Recipe: Scepter": "11", "Recipe: Shield": "12", "Recipe: Shortbow": "13", "Recipe: Staff": "14", "Recipe: Sword": "10", "Recipe: Torch": "16", "Recipe: Warhorn": "13", "Recipe: Trident": "16", "Recipe: Harpoon Gun": "8", "Recipe: Spear": "10", "Recipe: Light Coat": "15", "Recipe: Light Leggings": "6", "Recipe: Light Gloves": "13", "Recipe: Light Boots": "10", "Recipe: Light Helm": "10", "Recipe: Light Shoulders": "7", "Recipe: Medium Coat": "10", "Recipe: Medium Leggings": "5", "Recipe: Medium Gloves": "12", "Recipe: Medium Boots": "14", "Recipe: Medium Helm": "10", "Recipe: Medium Shoulders": "10", "Recipe: Heavy Coat": "9", "Recipe: Heavy Leggings": "17", "Recipe: Heavy Gloves": "6", "Recipe: Heavy Boots": "9", "Recipe: Heavy Helm": "6", "Recipe: Heavy Shoulders": "11" },
    { "total": "23500", "relics": "1542", "keys": "2267", "Vial of Potent Blood": "8000", "Large Bone": "8265", "Large Claw": "8125", "Large Scale": "7890", "Large Fang": "7920", "Intricate Totem": "7845", "Potent Venom Sac": "7990", "Pile of Incandescent Dust": "8105", "+1 Agony Infusion": "53317", "Dragonite Ore": "24400", "Empyreal Fragment": "22880", "Pile of Bloodstone Dust": "23230", "gear": "5", "mew": "501", "Manuscripts": "6662", "Proofs": "10098", "Treatises": "6792", "Postulates": "6655", "Aetherized Axe Skin": "1", "Aetherized Dagger Skin": "1", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "1", "Aetherized Hammer Skin": "1", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "1", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "1", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "10", "Recipe: Dagger": "6", "Recipe: Focus": "5", "Recipe: Greatsword": "1", "Recipe: Hammer": "3", "Recipe: Longbow": "4", "Recipe: Mace": "6", "Recipe: Pistol": "4", "Recipe: Rifle": "3", "Recipe: Scepter": "", "Recipe: Shield": "3", "Recipe: Shortbow": "7", "Recipe: Staff": "4", "Recipe: Sword": "2", "Recipe: Torch": "3", "Recipe: Warhorn": "9", "Recipe: Trident": "7", "Recipe: Harpoon Gun": "7", "Recipe: Spear": "5", "Recipe: Light Coat": "3", "Recipe: Light Leggings": "1", "Recipe: Light Gloves": "6", "Recipe: Light Boots": "2", "Recipe: Light Helm": "4", "Recipe: Light Shoulders": "3", "Recipe: Medium Coat": "3", "Recipe: Medium Leggings": "1", "Recipe: Medium Gloves": "2", "Recipe: Medium Boots": "8", "Recipe: Medium Helm": "6", "Recipe: Medium Shoulders": "4", "Recipe: Heavy Coat": "3", "Recipe: Heavy Leggings": "5", "Recipe: Heavy Gloves": "6", "Recipe: Heavy Boots": "5", "Recipe: Heavy Helm": "7", "Recipe: Heavy Shoulders": "5" },
    { "total": "37000", "relics": "2435", "keys": "3689", "Vial of Potent Blood": "12230", "Large Bone": "12505", "Large Claw": "13180", "Large Scale": "12605", "Large Fang": "12520", "Intricate Totem": "12125", "Potent Venom Sac": "12340", "Pile of Incandescent Dust": "12415", "+1 Agony Infusion": "83951", "Dragonite Ore": "37615", "Empyreal Fragment": "37715", "Pile of Bloodstone Dust": "36555", "gear": "13", "mew": "747", "Manuscripts": "10610", "Proofs": "16019", "Treatises": "10586", "Postulates": "10476", "Aetherized Axe Skin": "2", "Aetherized Dagger Skin": "1", "Aetherized Focus Skin": "1", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "1", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "1", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "1", "Recipe: Axe": "3", "Recipe: Dagger": "7", "Recipe: Focus": "9", "Recipe: Greatsword": "9", "Recipe: Hammer": "10", "Recipe: Longbow": "7", "Recipe: Mace": "9", "Recipe: Pistol": "10", "Recipe: Rifle": "6", "Recipe: Scepter": "11", "Recipe: Shield": "4", "Recipe: Shortbow": "7", "Recipe: Staff": "5", "Recipe: Sword": "8", "Recipe: Torch": "11", "Recipe: Warhorn": "7", "Recipe: Trident": "4", "Recipe: Harpoon Gun": "11", "Recipe: Spear": "6", "Recipe: Light Coat": "5", "Recipe: Light Leggings": "5", "Recipe: Light Gloves": "10", "Recipe: Light Boots": "6", "Recipe: Light Helm": "6", "Recipe: Light Shoulders": "6", "Recipe: Medium Coat": "6", "Recipe: Medium Leggings": "9", "Recipe: Medium Gloves": "7", "Recipe: Medium Boots": "4", "Recipe: Medium Helm": "9", "Recipe: Medium Shoulders": "6", "Recipe: Heavy Coat": "7", "Recipe: Heavy Leggings": "10", "Recipe: Heavy Gloves": "9", "Recipe: Heavy Boots": "5", "Recipe: Heavy Helm": "12", "Recipe: Heavy Shoulders": "11" },
    { "total": "50000", "relics": "3355", "keys": "4998", "Vial of Potent Blood": "16960", "Large Bone": "16985", "Large Claw": "16715", "Large Scale": "17010", "Large Fang": "16220", "Intricate Totem": "17355", "Potent Venom Sac": "17020", "Pile of Incandescent Dust": "16570", "+1 Agony Infusion": "113480", "Dragonite Ore": "48680", "Empyreal Fragment": "51365", "Pile of Bloodstone Dust": "51730", "gear": "7", "mew": "996", "Manuscripts": "14336", "Proofs": "21109", "Treatises": "14331", "Postulates": "14451", "Aetherized Axe Skin": "", "Aetherized Dagger Skin": "1", "Aetherized Focus Skin": "", "Aetherized Greatsword Skin": "", "Aetherized Hammer Skin": "", "Aetherized Longbow Skin": "", "Aetherized Mace Skin": "2", "Aetherized Pistol Skin": "", "Aetherized Rifle Skin": "", "Aetherized Scepter Skin": "1", "Aetherized Shield Skin": "", "Aetherized Short Bow Skin": "1", "Aetherized Staff Skin": "", "Aetherized Sword Skin": "", "Aetherized Torch Skin": "", "Aetherized Warhorn Skin": "1", "Aetherized Trident Skin": "", "Aetherized Harpoon Gun Skin": "", "Aetherized Spear Skin": "", "Recipe: Axe": "11", "Recipe: Dagger": "12", "Recipe: Focus": "6", "Recipe: Greatsword": "10", "Recipe: Hammer": "9", "Recipe: Longbow": "13", "Recipe: Mace": "7", "Recipe: Pistol": "21", "Recipe: Rifle": "11", "Recipe: Scepter": "9", "Recipe: Shield": "13", "Recipe: Shortbow": "13", "Recipe: Staff": "8", "Recipe: Sword": "12", "Recipe: Torch": "10", "Recipe: Warhorn": "8", "Recipe: Trident": "17", "Recipe: Harpoon Gun": "11", "Recipe: Spear": "8", "Recipe: Light Coat": "7", "Recipe: Light Leggings": "10", "Recipe: Light Gloves": "8", "Recipe: Light Boots": "5", "Recipe: Light Helm": "8", "Recipe: Light Shoulders": "5", "Recipe: Medium Coat": "16", "Recipe: Medium Leggings": "14", "Recipe: Medium Gloves": "11", "Recipe: Medium Boots": "10", "Recipe: Medium Helm": "15", "Recipe: Medium Shoulders": "8", "Recipe: Heavy Coat": "15", "Recipe: Heavy Leggings": "14", "Recipe: Heavy Gloves": "8", "Recipe: Heavy Boots": "4", "Recipe: Heavy Helm": "12", "Recipe: Heavy Shoulders": "8"}
]


// one tool to render the above data
interface FractalOutputs_Props {
    processed: Processed
}
interface FractalOutputs_State {
    processed: Processed
    // each component is responsible for its own number box
    boxValue: number
    visible: {
        [propName: string]: boolean
    }
}
// this one just sisplays the expected output in raw numbers
class FractalOutputs extends Component<FractalOutputs_Props, FractalOutputs_State> {
    constructor(props: FractalOutputs_Props) {
        super(props)
        this.state = {
            processed: this.props.processed,
            boxValue: 0,
            visible:{}
        }
    }

    onChange = (e:any) => {
        let value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        this.setState({ boxValue: value })
    }

    // create table

    createTable = () =>{
        let {processed, boxValue, visible} = this.state
        let {totalsAverage, valuesAverage, valuesTaxAverage} = processed

        if(typeof totalsAverage === "undefined"){
            return null
        }
        if(typeof valuesAverage === "undefined"){
            return null
        }
        if(typeof valuesTaxAverage === "undefined"){
            return null
        }

        // lets tey the categories first
        let rows = []

        for (let [key, value_tmp] of Object.entries(totalsAverage)) {
            if(typeof value_tmp === "number"){
                rows.push(
                    <tr>
                        <td style={{ textAlign: "left" }}>{capitalize(key)}</td>
                        <td style={{ textAlign: "right" }}>{(value_tmp*boxValue).toFixed(2)}</td>
                        <td style={{ textAlign: "right" }}>&nbsp;<GetCurrency number={valuesAverage[key]*boxValue} size={25} /> </td>
                        <td style={{ textAlign: "right" }}>&nbsp;<GetCurrency number={valuesTaxAverage[key]*boxValue} size={25} /> </td>
                    </tr>
                )
            }
            if(typeof value_tmp === "object"){
                let value: Processed_Sub_object = value_tmp
                rows.push(
                    <tr>
                        <td
                            style={{ textAlign: "left", cursor: "pointer" }}
                            onClick={()=>{this.show_hide(key)}}
                        ><b>{capitalize(key)}</b></td>
                        <td style={{ textAlign: "right" }}>{(value.total*boxValue).toFixed(2)}</td>
                        <td style={{ textAlign: "right" }}>&nbsp;<GetCurrency number={valuesAverage[key].total*boxValue} size={25} /></td>
                        <td style={{ textAlign: "right" }}>&nbsp;<GetCurrency number={valuesTaxAverage[key].total*boxValue} size={25} /></td>
                    </tr>
                )


                if(visible[key]){
                    for (let [key_sub, value_sub] of Object.entries(value)) {
                        rows.push(
                            <tr>
                                <td style={{ textAlign: "left" }}>{capitalize(key_sub)}</td>
                                <td style={{ textAlign: "right" }}>{(value_sub*boxValue).toFixed(2)}</td>
                                <td style={{ textAlign: "right" }}>&nbsp;<GetCurrency number={valuesAverage[key][key_sub]*boxValue} size={25} /></td>
                                <td style={{ textAlign: "right" }}>&nbsp;<GetCurrency number={valuesTaxAverage[key][key_sub]*boxValue} size={25} /></td>
                            </tr>
                        )
                    }
                }
            }

        }


        return <table className={"centerTable table-primary table-striped table-highlight"} >
            <thead>
            <tr>
                <th style={{ textAlign: "left" }}>Boxes</th>
                <th colSpan={3}><input style={{margin:0, width:400, textAlign: "right"}} type={"number"} value={boxValue} onChange={this.onChange}/></th>
            </tr>
            <tr>
                <th style={{ textAlign: "left" }}>Item</th>
                <th style={{ textAlign: "right" }}>Quantity</th>
                <th style={{ textAlign: "right" }}>Value</th>
                <th style={{ textAlign: "right" }}>Value (-Tax)</th>
            </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>



    }

    show_hide = (key: string) =>{
        let {visible} = this.state
        visible[key] = typeof visible[key] === "undefined" || !visible[key];
        this.setState({visible: visible})
    }

    render() {
        return this.createTable()
    }
}

interface FractalCost_Props {
    processed: Processed
    itemData: Objects[]
}
interface FractalCost_State {
    boxValue: number
    processed: Processed
    itemData: Objects[]
}
class FractalCost extends Component<FractalCost_Props, FractalCost_State> {
    constructor(props: FractalCost_Props) {
        super(props)
        this.state = {
            itemData: this.props.itemData,
            processed: this.props.processed,
            boxValue: 0,
        }
    }

    onChange = (e:any) => {
        let value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        this.setState({ boxValue: value })
    }

    // create table
    createTable = () =>{
        let {boxValue, processed, itemData} = this.state
        let key_chance = 0
        if(processed.totalsAverage){
            key_chance = processed.totalsAverage.keys
        }

        // infinite series condenses down to this
        // 1 + 1/x^1 + ... + 1/x^n
        let boxesToOpen = (1/(1-key_chance)) * boxValue

        let discounted = 0
        if(boxValue <= 30){
            discounted += boxValue * 2000
        }else if(boxValue <= 60){
            discounted += 30 * 2000
            discounted += (boxValue -30) * 2504
        }else{
            discounted += 30 * 2000
            discounted += 30 * 2504
            discounted += (boxValue - 60) * 3000
        }

        let matrix = 0
        let matrix_tmp = itemData.filter(item => item.name === "Stabilizing Matrix")
        if(matrix_tmp.length >0){
            matrix = matrix_tmp[0].buy_price
        }

        let box_price = 0
        let box_price_tmp = itemData.filter(item => item.name === "Fractal Encryption")
        if(box_price_tmp.length >0){
            box_price = box_price_tmp[0].buy_price
        }
        box_price = box_price * boxesToOpen

        return <table className={"centerTable table-primary table-striped table-highlight"} >
            <thead>
                <tr>
                    <th style={{ textAlign: "left" }}>Keys</th>
                    <th colSpan={2}><input style={{margin:0, width:400, textAlign: "right"}} type={"number"} value={boxValue} onChange={this.onChange}/></th>
                </tr>
                <tr>
                    <td style={{ textAlign: "center" }} colSpan={3}>~10% chance of getting a key from a box</td>
                </tr>
                <tr>
                    <td style={{ textAlign: "center" }} colSpan={3}>{boxValue} keys will open ~{boxesToOpen.toFixed(1)} boxes</td>
                </tr>
                <tr>
                    <th style={{ textAlign: "left" }}>Item</th>
                    <th style={{ textAlign: "right" }}>Cost</th>
                    <th style={{ textAlign: "right" }}>Total</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <th style={{ textAlign: "left" }}>Fractal Encryption's</th>
                    <td style={{ textAlign: "right" }}><GetCurrency number={box_price} size={25} /></td>
                    <td style={{textAlign: "right"}}/>
                </tr>
                <tr>
                    <th style={{ textAlign: "left" }}>Vendor</th>
                    <td style={{ textAlign: "right" }}><GetCurrency number={3000*boxValue} size={25} /></td>
                    <th style={{ textAlign: "right" }}><GetCurrency number={(3000*boxValue)+ box_price} size={25} /></th>
                </tr>
                <tr>
                    <th style={{ textAlign: "left" }}>Vendor (discount)</th>
                    <td style={{ textAlign: "right" }}><GetCurrency number={discounted} size={25} /></td>
                    <th style={{ textAlign: "right" }}><GetCurrency number={discounted+ box_price} size={25} /></th>
                </tr>
                <tr>
                    <th style={{ textAlign: "left" }}>Matrix</th>
                    <td style={{ textAlign: "right" }}><GetCurrency number={matrix*boxValue} size={25} /></td>
                    <th style={{ textAlign: "right" }}><GetCurrency number={(matrix*boxValue)+ box_price} size={25} /></th>
                </tr>
            </tbody>
        </table>
    }

    render() {
        return this.createTable()
    }
}