import React, { Component } from 'react'
import { graphOptions, urlBase } from '../_utiilities/data.json'
import custom from 'silveress_custom/silveress_custom'
import nesting from 'gw2e-recipe-nesting/src/'
import {cheapestTree, usedItems, updateTree} from 'gw2e-recipe-calculation'
import { getRarityColour } from '../_utiilities/functions'

import dateFormat from 'dateformat'
import Highcharts from 'highcharts/highstock'
import {
  Chart,
  ColumnSeries,
  HighchartsStockChart,
  Legend,
  Navigator,
  RangeSelector,
  Tooltip,
  withHighcharts,
  XAxis,
  YAxis,
} from 'react-jsx-highstock'
import {
  AutoComplete,
  Documentation,
  Loading,
  SilverTable,
} from '../_utiilities/functions_react'

let startTimer = Date.now()
let timer = {}
timer.start = (Date.now() - startTimer) / 1000

function isEqual(value, other) {

  // Get the value type
  let type = Object.prototype.toString.call(value);

  // If the two objects are not the same type, return false
  if (type !== Object.prototype.toString.call(other)) return false;

  // If items are not an object or array, return false
  if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

  // Compare the length of the length of the two items
  let valueLen = type === '[object Array]' ? value.length : Object.keys(
    value).length;
  let otherLen = type === '[object Array]' ? other.length : Object.keys(
    other).length;
  if (valueLen !== otherLen) return false;

  // Compare two items
  let compare = function (item1, item2) {

    // Get the object type
    let itemType = Object.prototype.toString.call(item1);

    // If an object or array, compare recursively
    if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
      if (!isEqual(item1, item2)) return false;
    }

    // Otherwise, do a simple comparison
    else {

      // If the two items are not the same type, return false
      if (itemType !== Object.prototype.toString.call(item2)) return false;

      // Else if it's a function, convert to a string and compare
      // Otherwise, just compare
      if (itemType === '[object Function]') {
        if (item1.toString() !== item2.toString()) return false;
      } else {
        if (item1 !== item2) return false;
      }

    }
  };

  // Compare properties
  if (type === '[object Array]') {
    for (let i = 0; i < valueLen; i++) {
      if (compare(value[i], other[i]) === false) return false;
    }
  } else {
    for (let key in value) {
      if (value.hasOwnProperty(key)) {
        if (compare(value[key], other[key]) === false) return false;
      }
    }
  }

  // If nothing failed, return true
  return true;
}

class TpItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      siteAddress: window.location.protocol + "//" + window.location.host,
      ids: []
    }
  }

  async loadData(url) {return await fetch(url).then(response => {return response.json()}).catch(err => console.log(this.props.url, err.toString()))}

  async componentDidMount() {
    timer.mounted = (Date.now() - startTimer) / 1000

    // get itemData
    this.itemData()

    // get recipes
    this.recipes()
  }

  // function for general item data
  itemData = async() =>{
    let result = await this.loadData(urlBase.parser+ "/v1/items/json?fields=id,name,img,rarity,chat_link,buy_price,sell_price,marketable&beautify=min&filter=")
    timer.itemData = (Date.now() - startTimer) / 1000;
    if(result.length === 0){
      this.setState({itemData: undefined})
    }else{
      let tmp = {}
      let sellPrices = {}
      let buyPrices = {}
      for (let i = 0; i < result.length; i++) {
        tmp[result[i].id] = result[i]

        if(typeof result[i].sell_price !== "undefined"){
          sellPrices[result[i].id] = result[i].sell_price
        }
        if(typeof result[i].buy_price !== "undefined"){
          buyPrices[result[i].id] = result[i].buy_price
        }
      }

      this.setState({itemData: tmp, itemDataRaw: result, prices:{buy:buyPrices,sell:sellPrices}})
    }
  }

  recipes = async () =>{
    let result = await this.loadData("https://api.datawars2.ie/gw2/v2/recipes?beautify=min")
    timer.recipes = (Date.now() - startTimer) / 1000;
    if(result.length === 0){
      this.setState({recipes: undefined})
    }else{
      // these are ecto "recipes" from achievements
      let blacklist = [-1278, -1926]
      let tmp = {}
      result = result.filter(item => blacklist.indexOf(item.id) === -1)
      for (let i = 0; i < result.length; i++) {
        tmp[result[i].id] = result[i]
      }
      this.setState({recipes: tmp, recipesRaw: result})
    }
  }

  history = async (ids) =>{
    ids = ids.filter(id =>  this.state.itemData[id].marketable)
    let result = await this.loadData(urlBase.parser+ "/v2/history/json?beautify=min&fields=date,itemID,buy_price_avg,sell_price_avg,buy_price_max,sell_price_min&itemID="+ids.join(","))
    timer.historyData = (Date.now() - startTimer) / 1000;
    return result
  }

  dropDown = () => {
    let recipes = this.state.recipesRaw
    let itemData = this.state.itemData


    let dropdownArray = []
    let counter = {}

    for(let i=0;i<recipes.length;i++){
      let tmp = {}
      tmp.id = recipes[i].id


      tmp.itemID = recipes[i].output_item_id
      let itemDetails = itemData[tmp.itemID]
      if(typeof itemDetails === "undefined"){
        //console.log(tmp.itemID, tmp.id)
        continue
      }
      tmp.itemName = itemDetails.name

      if(typeof counter[tmp.itemID] === "undefined"){
        counter[tmp.itemID] = 0
      }
      counter[tmp.itemID]++

      let count = ""
      if(counter[tmp.itemID] > 1){
        count = " ("+counter[tmp.itemID] + ")"
      }
      tmp.displayName = tmp.itemName + count

      dropdownArray.push(tmp)
    }

    return <AutoComplete
      identifier={"mainDropdown"}
      array={dropdownArray}
      callbackToParent={this.myCallback}
      sorter={"displayName"}
      placeholder={"Recipes Name"}
      startRenderString={"2"}
      limit={50}
    />
  }

  myCallback = async ([,,result]) => {
    let recipe = this.state.recipes[result.id]
    if(typeof recipe === "undefined"){return null}

    this.setState({recipe: recipe})
  }

  detailsTable = (itemData, recipe) =>{
    let square = 25
    let name = itemData.name;
    let rarity = itemData.rarity;
    let rarityColour = getRarityColour(rarity)

    let image = <img
      key={name}
      style={{ width: square,height:square, border: "1px solid "+ rarityColour }}
      src={itemData.img}
      title={name}
      alt={name}
    />

    let style0 = {}
    style0["min-width"] = 100 + "px"
    style0["width"] = 100 + "px"

    style0["text-align"] = "left"

    let style2 = {}
    style2["min-width"] = 100 + "px"
    style2["width"] = 100 + "px"
    style2["text-align"] = "right"

    let icons = []
    for(let i=0;i<recipe.disciplines.length;i++){
      let name = recipe.disciplines[i]
      let src

      switch(name){
        case "Artificer":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/0D75999D6DEA1FDFF9DB43BBC2054B62764EB9A0/102463.png"
          break
        }
        case "Weaponsmith":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/AEEF1CF774EE0D5917D5E1CF3AAC269FEE5EC03A/102460.png"
          break
        }
        case "Scribe":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/F95DFA3FBDCC9E9F317551A903E5A2A6DF1CC7E3/1293677.png"
          break
        }
        case "Huntsman":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/0C91017241F016EF35A2BCCE183CA9F7374023FC/102462.png"
          break
        }

        case "Leatherworker":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/192D1D0D73BA7899F1745F32BAC1634C1B4671BF/102464.png"
          break
        }
        case "Armorsmith":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/2952B92FA93C03A5281F94D223A4CE4C7E0B0906/102461.png"
          break
        }
        case "Tailor":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/0EB64958BE48AB9605DD56807713215095B8BEED/102459.png"
          break
        }
        case "Jeweler":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/F97F4D212B1294052A196734C71BCE42E199735B/102458.png"
          break
        }
        case "Chef":{
          src = "https://darthmaim-cdn.de/gw2treasures/icons/424E410B90DE300CEB4A1DE2AB954A287C7A5419/102465.png"
          break
        }
        default:{src = undefined}
      }

      icons.push(<img
        key={name}
        style={{ width: square, height: square }}
        src={src}
        title={name}
        alt={name}
      />)
    }

    return  <div>
      <h4 title={name + " - " + rarity} style={{color: rarityColour}}>
        {image} {name}
      </h4>
      <table className={"centerTable table-primary table-striped table-highlight"} >
        <tbody>
        <tr>
          <td style={style0}>Links</td>
          <td style={style2}>
            <a href={"http://wiki.guildwars2.com/wiki/?search=" + encodeURIComponent(itemData.chat_link)} target={"_blank"} rel={"noreferrer noopener"} title={"Wiki page for " + itemData.name }>𝐖iki</a> <br />
            <a href={"https://api.guildwars2.com/v2/items?ids=" + itemData.id} target={"_blank"} rel={"noreferrer noopener"} title={"API page for " + itemData.name }>API</a> <br />
            <a href={"https://www.gw2bltc.com/en/item/" + itemData.id} target={"_blank"} rel={"noreferrer noopener"} title={"Gw2BLTC page for " + itemData.name }>Gw2BLTC</a>
          </td>
        </tr>
        <tr>
          <td style={style0}>Professions</td>
          <td style={style2}><span title={recipe.disciplines.join()}>{icons}</span></td>
        </tr>
        <tr>
          <td style={style0}>Output Quantity</td>
          <td style={style2}>{recipe.output_item_count}</td>
        </tr>
        <tr>
          <td style={style0}>Level</td>
          <td style={style2}>{recipe.min_rating}</td>
        </tr>
        <tr>
          <td style={style0}>Crafting Time</td>
          <td style={style2}>{recipe.time_to_craft_ms/1000}s</td>
        </tr>
        <tr>
          <td style={style0}>Type</td>
          <td style={style2}>{recipe.type}</td>
        </tr>
        </tbody>
      </table>
    </div>
  }

  craftingBaseData = ()=>{
    let recipes = this.state.recipesRaw
    let recipe = this.state.recipe
    let prices = this.state.prices
    let itemData = this.state.itemData

    let nestedRecipes = nesting(recipes)

    let selectedRecipeTree = nestedRecipes.filter(item => item.recipeID === recipe.id)
    if(selectedRecipeTree.length !== 1 ){return [null, null, null, null, null]}

    // calculate both trees, these will be used again for each pointon the graph
    let sellTree = cheapestTree(1, selectedRecipeTree[0], prices.sell)
    let buyTree = cheapestTree(1, selectedRecipeTree[0], prices.buy)
    // get items from each
    let sellUsedItems = usedItems(sellTree).buy
    let buyUsedItems = usedItems(buyTree).buy
    // merge items
    let merged = Object.assign({},sellUsedItems,  buyUsedItems)
    let ids = Object.keys(merged)
    // filter out all non marketable

    ids = ids.filter((id)=> typeof itemData[id] !== "undefined" && typeof itemData[id].sell_price !== "undefined")

    // add in the crafted item's id if it can be sold
    if(typeof itemData[recipe.output_item_id] !== "undefined" && typeof itemData[recipe.output_item_id].sell_price !== "undefined"){
      if(ids.indexOf(recipe.output_item_id) === -1){
        ids.push(recipe.output_item_id)
      }
    }

    return [sellTree, buyTree, ids, buyUsedItems, sellUsedItems]
  }

  calculateCraftingData = () =>{
    // do recipe calcualtion first
    let [sellTree, buyTree, ids, buyUsedItems, sellUsedItems] = this.craftingBaseData()
    //this.craftingBaseData2()
    if(sellTree === null ){return [null,null]}

    // tables for the ingredients, buy and sell
    let [buyTable, sellTable] = this.ingredientsTableController(ids, buyUsedItems, sellUsedItems)

    // add ids to teh state
    if(!isEqual(this.state.ids, ids)){
      this.history(ids).then((result)=>{
        this.setState({ids:ids, history: result})
      })
    }

    return [buyTable, sellTable, sellTree, buyTree, ids]
  }

  processHistoryData = (history, sellTree, buyTree, ids) =>{
    let tmp = {}
    let processedData = {buy:[],sell:[],craftBuy:[],craftSell:[],profitMax:[],profitBuy:[],profitSell:[]};
    let craftedItemID = ids[ids.length-1]

    for(let i=0;i<history.length;i++){
      if(typeof tmp[history[i].date] === "undefined"){
        tmp[history[i].date] = {
          count :0 ,
          prices: {buy:{}, sell:{}}
        }
      }
      if(typeof tmp[history[i].date][history[i].itemID] === "undefined"){
        let sell = 0
        let buy = 0

        //sell_price_avg, sell_price_min
        if(typeof history[i].sell_price_avg !== "undefined"){
          sell = history[i].sell_price_avg
        }else if(typeof history[i].sell_price_min !== "undefined"){
          sell = history[i].sell_price_min
        }else{
          continue
        }

        // buy_price_avg buy_price_max
        if(typeof history[i].buy_price_avg !== "undefined"){
          buy = history[i].buy_price_avg
        }else if(typeof history[i].buy_price_max !== "undefined"){
          buy = history[i].buy_price_max
        }else{
          continue
        }

        tmp[history[i].date][history[i].itemID] = ""
        tmp[history[i].date].prices.sell[history[i].itemID] = sell
        tmp[history[i].date].prices.buy[history[i].itemID] = buy
      }
    }

    let dates = Object.keys(tmp)
    dates.sort()

    for(let i=0;i<dates.length;i++){
      let item = tmp[dates[i]]

      if(typeof item[ids[ids.length -1]] === "undefined"){continue}

      let craftingSell = updateTree(1, sellTree, item.prices.sell)
      let craftingBuy = updateTree(1, buyTree, item.prices.buy)

      let temp = {
        date: dates[i],
        buy: item.prices.buy[craftedItemID],
        sell: item.prices.sell[craftedItemID],

        craftBuy:craftingBuy.craftPrice,
        craftSell:craftingSell.craftPrice,
      }
      temp.profitMax = (temp.sell * 0.85) - temp.craftBuy
      temp.profitBuy = (temp.buy * 0.85) - temp.craftBuy
      temp.profitSell = (temp.sell * 0.85) - temp.craftSell

      let date = new Date(dates[i]).getTime();
      processedData.buy.push([date,Math.floor(temp.buy)]);
      processedData.sell.push([date,Math.floor(temp.sell)]);
      processedData.craftBuy.push([date,Math.floor(temp.craftBuy)]);
      processedData.craftSell.push([date,Math.floor(temp.craftSell)]);
      processedData.profitMax.push([date,Math.floor(temp.profitMax)]);
      processedData.profitBuy.push([date,Math.floor(temp.profitBuy)]);
      processedData.profitSell.push([date,Math.floor(temp.profitSell)]);
    }

    return processedData
  }

  processGraph = (graphData) => {
    return <div>
      <HighchartsStockChart
        plotOptions={graphOptions.plotOptions}
        styledMode
      >
        <Chart/>
        <Legend/>

        <RangeSelector
          //selected={4}
        >
          <RangeSelector.Button count={2} type="day">3d</RangeSelector.Button>
          <RangeSelector.Button count={7} type="day">7d</RangeSelector.Button>
          <RangeSelector.Button count={1} type="month">1m</RangeSelector.Button>
          <RangeSelector.Button count={3} type="month">3m</RangeSelector.Button>
          <RangeSelector.Button count={12} type="month">1y</RangeSelector.Button>
          <RangeSelector.Button type="all">All</RangeSelector.Button>
          <RangeSelector.Input boxBorderColor="#7cb5ec"/>
        </RangeSelector>

        <Tooltip
          shared={true}
          split={false}
          useHTML={true}
          formatter={function () {
            let s = '<table>'
            s+= '<thead><tr><td>'+dateFormat(new Date(this.x), "yyyy-mm-dd ddd")+'</td></tr></thead>'
            s+= '<tbody>'
            for (let i = 0; i < this.points.length; i++) {
              s += '<tr><td><span style="color:' + this.points[i].color + '">\u25CF</span> ' + this.points[i].series.name + '</td><td style="text-align:right">'+custom.convertToGold(this.points[i].y)+'</td></tr>'
            }
            s+= '</tbody>'
            s+= '</table>'
            return s;
          }}
        />

        <XAxis
          //onSetExtremes={this.getDataFromNav} min={this.state.minDate} max={this.state.maxDate} type={"datetime"}
        />


        <YAxis
          id={"axis_avgPrice"}
          labels={{ enabled: false }}
        >
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Buy" id="buy" data={graphData.buy} visible={false} />
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Sell" id="sell" data={graphData.sell}/>
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Craft - Buy" id="craftBuy" data={graphData.craftBuy} visible={false}/>
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Craft - Sell" id="craftSell" data={graphData.craftSell} visible={false}/>
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Profit - Max" id="profitMax" data={graphData.profitMax}/>
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Profit - Buy" id="profitBuy" data={graphData.profitBuy} visible={false}/>
          <ColumnSeries dataGrouping={{type: 'column', approximation:"average"}} name="Profit - Sell" id="profitSell" data={graphData.profitSell} visible={false}/>
        </YAxis>

        <Navigator
          //xAxis={{ min: this.state.minDate, max: this.state.maxDate }}
        >
          <Navigator.Series seriesId="buy"/>
          <Navigator.Series seriesId="sell"/>
          <Navigator.Series seriesId="craftBuy"/>
          <Navigator.Series seriesId="craftSell"/>
          <Navigator.Series seriesId="profitMax"/>
          <Navigator.Series seriesId="profitBuy"/>
          <Navigator.Series seriesId="profitSell"/>
        </Navigator>
      </HighchartsStockChart>
    </div>;
  }

  ingredientsTableController = (ids, buyUsedItems, sellUsedItems) =>{
    return [this.ingredientsTable(ids, buyUsedItems, "Buy"),this.ingredientsTable(ids, sellUsedItems, "Sell")]
  }

  ingredientsTable = (ids, data, type) =>{
    let itemData = this.state.itemData

    let dataArray = []
    for(let i=0;i<ids.length;i++) {
      let details = itemData[ids[i]]
      if(typeof details === "undefined"){continue}

      let tmp = {}
      tmp.id = details.id
      tmp.name = details.name
      tmp.rarity = details.rarity
      tmp.img = details.img

      tmp.quantity = data[details.id]

      dataArray.push(tmp)
    }

    let square = 25

    let textWidth = 270
    let endWidth = 80

    let config = {
      className: {
        table: "centerTable table-primary table-striped table-highlight",
      },
      templates: {
        "textImg": {
          contents:(item)=>{
            let name = item.name;
            let rarity = item.rarity
            let rarityColour = getRarityColour(item)

            let image = <img
              key={name}
              style={{ width: square,height:square, border: "1px solid "+ rarityColour }}
              src={item.img}
              title={name}
              alt={name}
            />

            return <span title={name + " - " + rarity} style={{color: rarityColour}} >{image} {name}</span>
          },
          className: "left",
          width: textWidth,
        },
        "number": {
          className: "right",
          width: endWidth,
          sort: (a, b) => custom.sortGeneral(a, b),
          filter: (item, filter) => {
            if(typeof item !== "undefined" && typeof filter !== "undefined"){
              return custom.filterGeneral(item, filter)
            }else{
              return false
            }
          }
        },
      },
      sort:{col:"name", desc:false},
      showButtons: false,
      headers: {},
    }

    config.headers = {
      cols: [
        { template: "textImg", header: "Ingredients - "+ type, accessor: "name" },
        { template: "number", header: "Quantity", accessor: "quantity" },
      ]
    }

    return <div>
      <div
        //style={{height: "600px",width: "180px", "overflow-y": "scroll"}}
        style={
          {width: "100%",
            height: "500px",
            overflow: "hidden"
          }
        }
      >
        <div
          style={
            {
              width: "100%",
              height: "100%",
              "overflow-y": "scroll",
              "padding-right": "17px", /* Increase/decrease this value for cross-browser compatibility */
              "box-sizing": "content-box" /* So the width will be 100% + 17px */
            }
          }
        >
          <SilverTable
            data={dataArray}
            config={config}
          />
        </div>
      </div>
    </div>
  }

  render() {
    //console.log(this.state)
    if(
      typeof this.state.itemData === "undefined"
      || typeof this.state.recipes === "undefined"
    ){
      return <Loading/>
    }
    let graph, dropdown, details, buyTable, sellTable, sellTree, buyTree, ids
    let tableArray = []

    let note = <p>
      <b>WARNING</b>: This page takes quite a bit of resources to run.
      <br/>
      For the graph to load it may take 4:30 min to download the data and another 1:30 min for processing the data.
      <br />
      Its basically <a href={"https://gw2efficiency.com/crafting/calculator/"} target={"_blank"} rel={"noreferrer noopener"} title={"Gw2Efficiency's Crafting page"}>Gw2Efficiency's Crafting page</a> (it does use some modules created by Gw2E) but calculated for every day back to 2012.
      <br />
      As a result the calculated prices will not be 100% accurate but rather should give ballpark figures as this uses teh current best recipe which the components may have not existed when the item was first released.
      <br />
      "Profit - Max" is crafted using buy price mats (cheapest) and selling at the sell price (most expensive) so it is roughly the best profit that can be made for that item for a given day.
    </p>

    dropdown = this.dropDown()

    if(typeof this.state.recipe !== "undefined"){
      let recipe = this.state.recipe
      let itemData = this.state.itemData[recipe.output_item_id]

      details = this.detailsTable(itemData,recipe);

      [buyTable, sellTable, sellTree, buyTree, ids] = this.calculateCraftingData();

      tableArray.push(details);
      tableArray.push(buyTable);
      tableArray.push(sellTable);
    }
    if(typeof this.state.history !== "undefined"){
      let processedData =this.processHistoryData(this.state.history, sellTree, buyTree, ids)
      graph = this.processGraph(processedData)
    }

    return <div style={{align:"center"}}>
      <Documentation url={"https://gitlab.com/Silvers_Gw2/Stats_Frontend/-/wikis/misc#historic-crafting"} />
      <br/>
      {note}
      <br />
      {dropdown}
      <br />
      <div className={"grid-results"} style={{  margin: "0 auto", "max-width":"1250px" }}>
        {tableArray}
      </div>
      {graph}
    </div>
  }
}

export default withHighcharts(TpItem, Highcharts)