import React, { Component } from 'react';
import custom from 'silveress_custom/silveress_custom'
import { urlBase } from '../_utiilities/data.json'
import { Documentation, GetCurrency, Loading, SilverTable, } from '../_utiilities/functions_react'

export default class Precursors extends Component {
    constructor (props) {
        super(props);
        this.state = { forgingData: [], priceData: [] }
    }

    async loadData (url) {return await fetch(url).then(response => response.json()).then(data => {return data;}).catch(err => console.error(this.props.url, err.toString()))}

    async componentDidMount () {
        let tmp = {};
        tmp.itemData = await this.loadData(urlBase.parser + "/v1/items/json?beautify=min&fields=description,year_buy_min,year_sell_min,id,name,buy_quantity,buy_price,sell_quantity,sell_price&filter=marketable:TRUE,type:Consumable,rarity:Rare,weaponType:Transmutation");
        tmp.output = this.reduceForgingData(tmp.itemData);
        this.setState(tmp)
    }

    reduceForgingData (itemData) {
        const exceptionNames = [
            "Lightning Catcher",
            "Toxic Spore",
            "Sun Catcher",
            "Desert Rose",
            "Wind Catcher",
            "Tiki Totem",
            "Shark's Tooth",
            "Consortium Clipper",
            "Zephyr Rucksack",
            "Chiroptophobia",
            "Araneae",
            "Dead Stop",
            "Bloody Prince",
            "Silly Scimitar",
            "Kaiser Snake",
          "Super"
        ];
        const toybox = [
            "Silly Scimitar Skin",
            "Dead Stop Shield Skin",
            "Bloody Prince Staff Skin",
            "Chiroptophobia Greatsword Skin",
            "Araneae Longbow Skin"];
        const weapons = [
            "Greatsword",
            "Staff",
            "Longbow",
            "Warhorn",
            "Dagger",
            "Torch",
            "Focus",
            "Hammer",
            "Scepter",
            "Axe",
            "Short",
            "Long",
            "Bow",
            "Rifle",
            "Mace",
            "Pistol",
            "Sword",
            "Shield",
            "Trident",
            "Aspect",
            "Spear",
            "Harpoon",
            "Gun",
            "Blunderbuss",
            "Cudgel",
            "Call",
            "Avenger",
            "Truncheon",
            "Warhammer",
            "Needler",
            "Anlace",
            "Flintlock",
            "Flame",
            "Hornbow",
            "Kris",
            "Sting",
            "Slice",
            "Brunt",
            "Edge",
            "Timber",
            "Harbinger",
            "Protector",
            "Cutter",
            "Shelter",
            "Wall",
            "Lacerator",
            "Reaver",
            "Needle",
            "Quarterstaff",
            "Reach",
            "Arc"];

        let output = [];

        function addToOutput (item, collection) {
            let objIndex = output.findIndex((x => x.collection === collection));

            if (objIndex === -1) {
                output.push({
                    collection: collection,
                    count: 0,
                    skins: [],
                    orderBuy: 0,
                    orderBuyMin: 0,
                    orderSellMin: 0,
                    instantBuy: 0
                });
                objIndex = output.findIndex((x => x.collection === collection));
            }

            output[objIndex].count = output[objIndex].count + 1;
            output[objIndex].skins.push(item.name);
            output[objIndex].orderBuy = output[objIndex].orderBuy +
              item.buy_price;
            if (item.year_buy_min <= item.buy_price) {
                output[objIndex].orderBuyMin = output[objIndex].orderBuyMin +
                  item.year_buy_min;
            } else {
                output[objIndex].orderBuyMin = output[objIndex].orderBuyMin +
                  item.buy_price;
            }
            if (item.year_sell_min <= item.sell_price) {
                output[objIndex].orderSellMin = output[objIndex].orderSellMin +
                  item.year_sell_min;
            } else {
                output[objIndex].orderSellMin = output[objIndex].orderSellMin +
                  item.sell_price;
            }

            output[objIndex].instantBuy = output[objIndex].instantBuy +
              item.sell_price;

            output[objIndex].orderAvg = output[objIndex].orderBuy /
              output[objIndex].count;
            output[objIndex].instantAvg = output[objIndex].instantBuy /
              output[objIndex].count;

            output[objIndex].tickets = 7;

            if (output[objIndex].count === 10) {
                output[objIndex].tickets = 5;
            }
            if (output[objIndex].count === 5) {
                output[objIndex].tickets = 5;
            }

            output[objIndex].ticketBuy = output[objIndex].orderBuy /
              output[objIndex].tickets;
            output[objIndex].ticketSell = output[objIndex].instantBuy /
              output[objIndex].tickets;

            // Formatting
            output[objIndex].ticketBuyFormatted = output[objIndex].ticketBuy
            output[objIndex].ticketSellFormatted = output[objIndex].ticketSell
            output[objIndex].orderBuyFormatted = output[objIndex].orderBuy
            output[objIndex].orderBuyMinFormatted = output[objIndex].orderBuyMin
            output[objIndex].orderSellMinFormatted = output[objIndex].orderSellMin
            output[objIndex].instantBuyFormatted = output[objIndex].instantBuy
        }

        for (let i = 0; i < itemData.length; i++) {
            let name = itemData[i].name;
            if (name.includes("Skin") || name.includes("skin")) {
                let collection0 = name.split(" ");
                let collection = [];

                for (let ii = 0; ii < collection0.length; ii++) {
                    let item = collection0[ii];
                    if (weapons.indexOf(item) === -1 && item !== "Skin") {
                        collection.push(item)
                    }
                }
                collection = collection.join(" ");
                if (exceptionNames.indexOf(collection) === -1) {
                    addToOutput(itemData[i], collection)
                }
                if (toybox.indexOf(name) !== -1) {
                    addToOutput(itemData[i], "Bloody Prince's Toybox")
                }
            }
        }
        return output;
    }

    createTable = (data) => {
        let config = {
            className: {
                table: "centerTable table-primary table-striped table-highlight",
            },
            templates: {
                "text": {
                    className: "left",
                },
                "number": {
                    className: "right",
                    sort: (a, b) => custom.sortNum(a, b),
                },
                "gold": {
                    className: "right",
                    contents:(item, accessor)=> <GetCurrency number={item[accessor]} size={25} />,
                    sort: (a, b) => custom.sortGold(a, b),
                }
            },
            headers: {
                "Collection Info": {
                    className: "center",
                    cols: [
                        { template: "text", header: "Collection", accessor: "collection" },
                        { template: "number", header: "Skins", accessor: "count" },
                        { template: "number", header: "Tickets", accessor: "tickets" }
                    ],
                },
                "Lowest Cost (Year)": {
                    collapse: true,
                    className: "center",
                    cols: [
                        { template: "gold", header: "Buy Order", accessor: "orderBuyMinFormatted" },
                        { template: "gold", header: "Instant Buy", accessor: "orderSellMinFormatted" },
                    ],
                },
                "Skin Cost": {
                    collapse: true,
                    className: "center",
                    cols: [
                        { template: "gold", header: "Buy Order", accessor: "orderBuyFormatted" },
                        { template: "gold", header: "Instant Buy", accessor: "instantBuyFormatted" },
                    ],
                },
                "Ticket Cost": {
                    collapse: true,
                    className: "center",
                    cols: [
                        { template: "gold", header: "Buy Order", accessor: "ticketBuyFormatted" },
                        { template: "gold", header: "Instant Buy", accessor: "ticketSellFormatted" },
                    ],
                },
            },
            sort: { col: "ticketBuyFormatted", desc: false },
            headerOrder:["Collection Info","Lowest Cost (Year)","Skin Cost","Ticket Cost"],
        }

        return <SilverTable
          data={data}
          config={config}
        />
    }

    render () {
        let output = this.state.output;
        if (typeof output === "undefined") {return <Loading/>}

        return <div style={{ align: "center" }}>
            <Documentation url={"https://gitlab.com/Silvers_Gw2/Stats_Frontend/-/wikis/misc#bltc-tickets"} />
            <br/>
            <h3>Completing BLTC weapon collections for tickets.</h3>
            <h5>Use in conjunction with <a href="https://gw2efficiency.com/currencies/claim-tickets" target="_blank" rel="noopener noreferrer">https://gw2efficiency.com/currencies/claim-tickets</a>
            </h5>
            <br/>
            {this.createTable(output)}
        </div>;
    }
}