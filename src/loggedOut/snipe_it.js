import React, { Component } from 'react';
import custom from 'silveress_custom/silveress_custom'
import { urlBase } from '../_utiilities/data.json'
import { Documentation, GetCurrency, Loading, SilverTable, } from '../_utiilities/functions_react'

export default class SnatchIt extends Component {
    constructor(props) {
        super(props);
        this.state = {apiData: [], tableData:[]};
    }

    async loadData(url) {
        return await fetch(url).then(response => response.json()).then(data => {return data;}).catch(err => console.error(this.props.url, err.toString()))
    }

    async componentDidMount() {
        let baseData = {};
        baseData.apiData = await this.loadData(urlBase.parser + "/v1/items/json?fields=id,name,1m_sell_sold,snipeIt&beautify=min");
        baseData.tableData = await SnatchIt.apiToTable(baseData.apiData);
        this.setState(baseData);
    }

    static async apiToTable(apiData){
        let tableData = []
        for(let i=0;i<apiData.length;i++){
            if(typeof apiData[i].snipeIt !== "undefined"){
                if(typeof apiData[i].snipeIt.buyMin === "undefined"){continue}

                let tmp = {};
                tmp.name        = apiData[i].name;
                tmp.buyMin      = apiData[i].snipeIt.buyMin;
                tmp.buyMax      = apiData[i].snipeIt.buyMax;
                tmp.buyPer      = (tmp.buyMax - tmp.buyMin)/tmp.buyMin;
                tmp.quantity    = apiData[i].snipeIt.quantity;
                tmp.sell        = apiData[i].snipeIt.sellPrice;

                tmp.value       = apiData[i].snipeIt.avgBuyPrice * tmp.quantity;
                tmp.profit      = ((tmp.sell * 0.85) - apiData[i].snipeIt.avgBuyPrice) * tmp.quantity;
                tmp.profitPer   = tmp.profit/(tmp.value);
                tmp.eta         = tmp.quantity/(apiData[i]["1m_sell_sold"]/30);
                tmp.profitDay   = tmp.profit/tmp.eta;

                if(tmp.eta !== Infinity){
                    tmp.profitPer   = tmp.profitPer.toFixed(2) + '%'
                    tmp.eta = tmp.eta.toFixed(3)
                    tmp.buyPer      = tmp.buyPer.toFixed(2) + '%'
                    tableData.push(tmp)
                }
            }
        }
        return tableData;
    }

    tableManager = (data) => {
        let config = {
            className: {
                table: "centerTable table-primary table-striped table-highlight",
            },
            templates: {
                "text": {
                    className: "left",
                },
                "number": {
                    className: "right",
                    sort: (a, b) => custom.sortNum(a, b),
                    filter: (item, filter) => custom.filterGeneral(item, filter)
                },
                "percent": {
                    className: "right",
                    sort: (a, b) => custom.sortPercent(a, b),
                    filter: (item, filter) => custom.filterGeneral(item, filter)
                },
                "gold": {
                    className: "right",
                    contents:(item, accessor)=> <GetCurrency number={item[accessor]} size={25} />,
                    sort: (a, b) => custom.sortGeneral(a, b),
                    filter: (item, filter) => custom.filterGeneral(item, filter, 10000)
                }
            },
            colsToDisplay:20,
            filter: {active:true},
            sort:{ col: "profit", desc: true },
            headers: {
                "": {
                    cols: [
                        { template: "text", header: "Name", accessor: "name" },
                    ]
                },
                "Buy": {
                    cols: [
                        { template: "gold", header: "From", accessor: "buyMin" },
                        { template: "gold", header: "To", accessor: "buyMax" },
                        { template: "gold", header: "% Difference", accessor: "buyPer" },
                        { template: "number", header: "Quantity", accessor: "quantity" },
                        { template: "gold", header: "Value", accessor: "value" },
                    ]
                },
                "Sell": {
                    cols: [
                        { template: "gold", header: "Price", accessor: "sell" },
                        { template: "number", header: "Days", accessor: "eta" },
                    ]
                },
                "Profits": {
                    cols: [
                        { template: "gold", header: "Profit", accessor: "profit" },
                        { template: "percent", header: "Profit %", accessor: "profitPer" },
                        { template: "gold", header: "Profit/Day", accessor: "profitDay" },
                    ]
                }
            },
            headerOrder:["","Buy", "Sell", "Profits"]
        }
        return this.createTable(data, config)
    }

    createTable = (data, config) => {
        return <SilverTable
          data={data}
          config={config}
        />
    }

    render() {
        if(this.state.tableData.length === 0){return <Loading/>}
        return <div>
            <Documentation url={"https://gitlab.com/Silvers_Gw2/Stats_Frontend/-/wikis/misc#snipe-it"} />
            <br/>
            <div><h3>Silveress's Snipe-it</h3></div>
            {this.tableManager(this.state.tableData)}
        </div>
    }
}