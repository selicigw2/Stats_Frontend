import React from "react";
import { Route, Switch } from "react-router-dom";

import { withTracker} from './_utiilities/functions_react'

import Home from "./loggedOut/home";
import precursors from "./loggedOut/precursors";
import precursors_stats from "./loggedOut/precursors_stats";
import gemstones from "./loggedOut/gemstones";
import bltc_tickets from "./loggedOut/bltc_tickets";
import minis from "./loggedOut/minis";
import tp from "./loggedOut/tp"
import tpDetailed from "./loggedOut/tp_detailed"
import tpGnashblade from "./loggedOut/tp_gnashblade"
import tpItem from "./loggedOut/tp_item"
import skin2Win from "./loggedIn/skin2win"
import snatchIt from "./loggedOut/snatch_it"
import snipeIt from "./loggedOut/snipe_it"
import statsCollection from "./loggedIn/stats_collection"
import statsViewer from "./loggedOut/stats_collection_viewer"
import friends from "./loggedOut/friends"
import account from "./loggedIn/A_account"
import transactionViewer from "./loggedIn/transactionViewer"
import lists from "./loggedIn/A_lists"
import monitoring from "./loggedOut/monitoring"
import wvwStats from "./loggedIn/wvwStats"
import inventoryToCSV from "./loggedIn/inventoryToCSV"
import support from "./loggedOut/support"
import refinement from "./loggedOut/refinement"
import freeBagSlots from "./loggedIn/freeBagSlots"
import verification_Lists from "./loggedIn/A_verification"
import verification from "./loggedOut/verification"
import buyout from "./loggedOut/buyout"
import accountToCSV from "./loggedIn/account_to_csv"
import mcStandard from "./loggedOut/mc_standard"
import altStandard from "./loggedOut/alt_standard"
import StoryTracker from "./loggedIn/storyTracker"
import RaidCurrency from "./loggedOut/raid_currency"
import Extracting from "./loggedOut/extracting"
import HistoricCrafting from "./loggedOut/historicCrafting"
import {Fractal_Main} from "./loggedOut/fractal_encryption"

import login from "./login/login"
import signup from "./login/signup"

import ResetUsername from "./recovery/username"
import ResetPassword from "./recovery/password"

import FarmingTracker from "./loggedIn/farmingTracker"

import VendorTrash from "./loggedOut/vendor_trash"
import GemStoreWaitlist from "./loggedIn/gemstore"

import Festival_Winds from "./loggedOut/festival_winds"

export default () => <Switch>
      <Route exact path="/" component={withTracker(Home)} />
      <Route path="/precursors" component={withTracker(precursors)} />
      <Route path="/precursors_stats" component={withTracker(precursors_stats)} />
      <Route path="/gemstones" component={withTracker(gemstones)} />
      <Route path="/bltc_tickets" component={withTracker(bltc_tickets)} />
      <Route path="/minis" component={withTracker(minis)} />
      <Route exact path="/tradepost" component={withTracker(tp)} />
      <Route exact path="/tradepost/detailed" component={withTracker(tpDetailed)} />
      <Route exact path="/tradepost/gnashblade" component={withTracker(tpGnashblade)} />
      <Route exact path="/tradepost/item/:id" component={withTracker(tpItem)} />
      <Route path="/skin_2_win" component={withTracker(skin2Win)} />
      <Route path="/snatch_it" component={withTracker(snatchIt)} />
      <Route path="/snipe_it" component={withTracker(snipeIt)} />
      <Route path="/stats_collection" component={withTracker(statsCollection)} />
      <Route path="/stats_viewer" component={withTracker(statsViewer)} />
      <Route path="/friends" component={withTracker(friends)} />
      <Route path="/login" component={withTracker(login)} />
      <Route path="/signup" component={withTracker(signup)} />
      <Route path="/account" component={withTracker(account)} />
      <Route path="/transaction_viewer" component={withTracker(transactionViewer)} />
      <Route path="/lists" component={withTracker(lists)} />
      <Route path="/monitoring" component={withTracker(monitoring)} />
      <Route path="/WvW_Stats" component={withTracker(wvwStats)} />
      <Route path="/inventory_to_csv" component={withTracker(inventoryToCSV)} />
      <Route path="/support" component={withTracker(support)} />
      <Route path="/refinement" component={withTracker(refinement)} />
      <Route path="/freeBagSlots" component={withTracker(freeBagSlots)} />
      <Route path="/verification_management" component={withTracker(verification_Lists)} />
      <Route path="/verification" component={withTracker(verification)} />
      <Route path="/buyout" component={withTracker(buyout)} />
      <Route path="/account_to_csv" component={withTracker(accountToCSV)} />
      <Route path="/mc_standard" component={withTracker(mcStandard)}/>
      <Route path="/alt_standard" component={withTracker(altStandard)}/>
      <Route path="/story_tracker" component={withTracker(StoryTracker)}/>
      <Route path="/raid_currency" component={withTracker(RaidCurrency)}/>
      <Route path="/extracting" component={withTracker(Extracting)}/>
      <Route path="/historicCrafting" component={withTracker(HistoricCrafting)}/>
      <Route path="/reset/username" component={withTracker(ResetUsername)}/>
      <Route path="/reset/password" component={withTracker(ResetPassword)}/>
      <Route path="/farmingTracker" component={withTracker(FarmingTracker)}/>
      <Route path="/vendor_trash" component={withTracker(VendorTrash)}/>
      <Route path="/gemstore_notifier" component={withTracker(GemStoreWaitlist)}/>
      <Route path="/festival_winds" component={withTracker(Festival_Winds)}/>
      <Route path="/fractal_encryption" component={withTracker(Fractal_Main)}/>
</Switch>