import React, {Component} from 'react';
import custom from 'silveress_custom/silveress_custom'
import { Button, Table } from 'react-bootstrap'
import cookie from "react-cookies";
import {Redirect} from "react-router-dom";
import JSONConverter from "silvers_json_converter"
import { urlBase } from '../_utiilities/data.json'
import { Documentation, GetCurrency, Loading, SilverTable, } from '../_utiilities/functions_react'

class InventoryToCSV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      session: cookie.load('session'),
      currencies: [],
      characterDropdown: "Choose Character",
      apiKey: undefined,
      itemData:{},
      firstBags:"",
      filter:[],
      firstSnapshotTime: ""
    };

    this.handleCharacterSelection = this.handleCharacterSelection.bind(this);
    this.handleCharacterSelection2 = this.handleCharacterSelection2.bind(this);
    this.handleFirstSnapshot = this.handleFirstSnapshot.bind(this);
  }

  async loadData(url,extra) {return await fetch(url,extra).then(response => {return response.json()}).catch((err) => {console.log(this.props.url, err.toString()); return {result:"error", error:"could not fetch"};})}

  async componentDidMount() {
    let tmp = {};
    let rawItemData = await this.loadData(urlBase.parser + "/v1/items/json?fields=type,img,id,name,level,rarity,marketable,binding,sell_price,weaponType,statName,charm&beautify=human&filter=id:gt:0");

    tmp.itemData = {};
    for(let i=0;i<rawItemData.length;i++){
      tmp.itemData[rawItemData[i].id] = rawItemData[i];
    }

    let account = await this.loadData(urlBase.account + "/v1/website/account",{method: 'GET', headers: {'Content-Type': 'application/json', session: this.state.session}});
    tmp.defaultAccountID = "";
    if(account.result === "success"){
      tmp.accountMain = account.account;
      let accounts = tmp.accountMain.gameAccounts;
      tmp.userIDs = Object.keys(accounts);
      if(tmp.userIDs.length >0){
        tmp.defaultAccountID = tmp.userIDs[0];
        tmp.apiKey = tmp.accountMain.gameAccounts[tmp.userIDs[0]].key;
      }
    }
    if(account.result ==="error"){
      if(account.error ==="Invalid session"){
        cookie.remove('session');
        tmp.session = undefined;
      }
    }

    if (typeof tmp.apiKey !== "undefined") {
      tmp = await this.onApiKey(tmp,tmp.apiKey);
    }
    this.setState(tmp);
  }

  async onApiKey(tmp,apiKey) {
    let accountData = await this.loadData("https://api.guildwars2.com/v2/account?access_token=" + apiKey);
    tmp.playerID = accountData.name;
    tmp.characters = await this.loadData("https://api.guildwars2.com/v2/characters?access_token=" + apiKey);
    return tmp;
  }

  async handleFirstSnapshot() {
    let tmp = {};
    tmp.firstSnapshotTime = new Date().toLocaleString();
    tmp.firstWallet = await this.loadData("https://api.guildwars2.com/v2/account/wallet?access_token=" + this.state.apiKey);
    tmp.character = await this.loadData("https://api.guildwars2.com/v2/characters/"+this.state.characterDropdown+"?access_token=" + this.state.apiKey);
    let bags = tmp.character.bags;
    bags = bags.filter(function (el) {return el != null;});
    let tempBag0 = {};
    for(let i=0;i<bags.length;i++){
      let bag = bags[i].inventory;
      for(let ii=0;ii<bag.length;ii++){
        if(bag[ii] !== null){
          let itemID = bag[ii].id;
          let itemCount = bag[ii].count;

          if(typeof tempBag0[itemID] === "undefined"){
            tempBag0[itemID] = 0;
          }
          tempBag0[itemID] += itemCount;
        }
      }
    }
    tmp.firstBags = tempBag0;
    this.setState(tmp)
  }

  async handleCharacterSelection(event){
    let tmp = {};
    tmp.characterDropdown = event.target.value;
    this.setState(tmp)
  }

  static getDataFromID(id, items){
    id = id-0;
    let data = {};
    let result = items[id];
    if(typeof result !== "undefined"){
      data = result
    }
    return data;
  }

  async handleCharacterSelection2(event) {
    let tmp = {};
    tmp.defaultAccountID = event.target.value;
    tmp.apiKey = this.state.accountMain.gameAccounts[event.target.value].key;
    tmp = await this.onApiKey(tmp,tmp.apiKey);
    this.setState(tmp);
  }


  bagsToTable(bags){
    let tableData = []
    let ids = Object.keys(bags)
    for(let i=0;i<ids.length;i++){
      let tmp = {}
      tmp.id= ids[i]
      let itemData = InventoryToCSV.getDataFromID(tmp.id, this.state.itemData)
      tmp.name = itemData.name
      tmp.quantity = bags[tmp.id]
      tmp.rarity = itemData.rarity
      tmp.img = itemData.img
      tmp.type = itemData.type
      tmp.level = itemData.level
      tmp.binding = itemData.binding
      tmp.weaponType = itemData.weaponType
      tmp.statName = itemData.statName
      tmp.charm = itemData.charm
      let price = itemData.sell_price || itemData.buy_price || 0
      tmp.value_taxed = tmp.quantity * price * 0.85
      tmp.value = tmp.quantity * price
      tableData.push(tmp)
    }
    return tableData
  }

  static processFilter(tmp, filter){
    for(let i=0;i<filter.length;i++){
      if(filter[i].id === "name"
        || filter[i].id === "id"
        || filter[i].id === "type"
        || filter[i].id === "rarity"
        || filter[i].id === "binding"
        || filter[i].id === "subType"
        || filter[i].id === "stat"
        || filter[i].id === "charm"
      ){
        tmp = tmp.filter((item)=>{
          let searchItem = item[filter[i].id] || ""
          searchItem  = searchItem.toString().toLowerCase()
          let searchTerm = filter[i].value || ""
          searchTerm = searchTerm.toString().toLowerCase()
          return searchItem.indexOf(searchTerm) !== -1
        })
      }else{
        tmp = tmp.filter((item)=> custom.filterGeneral (item, filter[i].value))
      }
    }
    return tmp;
  }

  getSelectedDetails = (input) => {
    let value = 0
    let value_taxed = 0
    let quantity = 0
    for (let i = 0; i < input.length; i++) {
      if (!isNaN(input[i].value_taxed)) {
        value_taxed += input[i].value_taxed
      }
      if (!isNaN(input[i].value)) {
        value += input[i].value
      }
      if (!isNaN(input[i].quantity)) {
        quantity += input[i].quantity
      }
    }
    return [value, value_taxed, quantity]
  }

  tableManager = (data) => {
    let square = 25
    let config = {
      className: {
        table: "centerTable table-primary table-striped table-highlight",
      },
      templates: {
        "text": {
          className: "left",
        },
        "textImg": {
          contents:(item)=>{
            let name = item.name;
            let image = <img
              key={name}
              style={{width: square,height:square}}
              src={item.img}
              title={name}
              alt={name}
            />;
            return <span title={name}>{image} {name}</span>
          },
          className: "left",
        },
        "number": {
          className: "right",
          sort: (a, b) => custom.sortGeneral(a, b),
          filter: (item, filter) => custom.filterGeneral(item, filter)
        },
        "percent": {
          className: "right",
          sort: (a, b) => custom.sortGeneral(a, b),
          filter: (item, filter) => custom.filterGeneral(item, filter)
        },
        "gold": {
          className: "right",
          contents:(item, accessor)=> <GetCurrency number={item[accessor]} size={25} />,
          sort: (a, b) => custom.sortGeneral(a, b),
          filter: (item, filter) => custom.filterGeneral(item, filter, 10000)
        }
      },
      colsToDisplay:20,
      filter: {active:true},
      sort:{col:"name", desc:true},
      headers: {
        "Details": {
          className:"left",
          cols: [
            { template: "textImg", header: "Item", accessor: "name" },
            { template: "number", header: "Quantity", accessor: "quantity" },
            { template: "gold", header: "Value", accessor: "value" },
            { template: "number", header: "ID", accessor: "id" }
          ]
        },
        "Filters": {
          collapse:true,
          className:"left",
          cols: [
            { template: "text", header: "Binding", accessor: "binding" },
            { template: "text", header: "Rarity", accessor: "rarity" },
            { template: "number", header: "Level", accessor: "level" },
            { template: "text", header: "Type", accessor: "type" },
            { template: "text", header: "Sub-Type", accessor: "weaponType" },
            { template: "text", header: "Stat", accessor: "statName" },
            { template: "text", header: "Charm", accessor: "charm" },

          ]
        }
      },
      headerOrder:["Details","Filters"]
    }

    return this.createTable(data, config)
  }

  createTable = (data, config) => {
    return <SilverTable
      data={data}
      config={config}
      callbackToParent={this.callbackToParent}
    />
  }

  callbackToParent = (item) =>{
    let filter = []
    let newFilters = Object.keys(item.filter)
    for(let i=0;i<newFilters.length;i++){
      filter.push({
        id:newFilters[i],
        value:item.filter[newFilters[i]]
      })
    }
    this.setState({filter:filter})
  }

  render() {
    console.log(this.state)
    if(typeof this.state.session === "undefined"){return <Redirect to='/login'  />;}
    if(typeof this.state.accountMain === "undefined"){return <Loading/>}

    let characterDropdown = []
    if(this.state.userIDs.length >0){
      characterDropdown.push(<option key={-1} value={this.state.defaultAccountID} >{this.state.defaultAccountID}</option>);
      for(let i=0;i<this.state.userIDs.length;i++){
        if(this.state.userIDs[i] !== this.state.defaultAccountID){
          characterDropdown.push(<option key={i} value={this.state.userIDs[i]}>{this.state.userIDs[i]}</option>)
        }
      }
    }
    let accountSelect = <select value={this.state.defaultAccountID} onChange={this.handleCharacterSelection2}>{characterDropdown}</select>

    let outputTable,infoTable, characterSelect, firstSnapshot
    if (typeof this.state.apiKey !== "undefined" && typeof this.state.characters !== "undefined") {
      let characterDropdown = [<option key="-1" value="">Select Character</option>]
      for(let i=0;i<this.state.characters.length; i++){
        characterDropdown.push(<option key={i} value={this.state.characters[i]}>{this.state.characters[i]}</option>)
      }
      characterSelect =  <form><select style={{width: 200}} value={this.state.characterDropdown} onChange={this.handleCharacterSelection}>{characterDropdown}</select></form>
    }

    if(this.state.characterDropdown !== "Choose Character"){
        firstSnapshot = (
          <div>
            <Button variant="primary" onClick={this.handleFirstSnapshot}>Snapshot</Button>
          </div>
        );
    }

    if (this.state.firstBags !== "") {
      let tableData = this.bagsToTable(this.state.firstBags)
      outputTable = this.tableManager(tableData)

      let headers = [
        {title:"ID",key:"id",type:"number"},
        {title:"Name",key:"name",type:"string"},
        {title:"Quantity",key:"quantity",type:"number"}]

      let csvData =  InventoryToCSV.processFilter(tableData,this.state.filter)
      let name = new Date().toISOString() +"_"+  this.state.characterDropdown

      let [value, value_taxed,  quantity] = this.getSelectedDetails(csvData)

      infoTable = <Table
        striped
        bordered
        size="sm"
        style={{ "table-layout":"auto", width:"300px", align:"center", margin: "0 auto" }}
        //style={{display: "inline-table"}}

      >
          <thead>
          <tr>
            <th className={"left"}>Value</th>
            <th className={"left"}>Tax</th>
            <th className={"left"}>Value-Tax</th>
            <th className={"left"}>Quantity</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td><GetCurrency number={value} size={25} /></td>
            <td><GetCurrency number={value-value_taxed} size={25} /></td>
            <td><GetCurrency number={value_taxed} size={25} /></td>
            <td>{quantity}</td>
          </tr>
          <tr>
            <td className={"left"}>Date</td>
            <td colSpan="3">{this.state.firstSnapshotTime}</td>
          </tr>
          <tr>
            <td className={"left"}>Download</td>
            <td className={"left"}><JSONConverter data={csvData} type={"JSON"} beautify={true} headers={headers} text={"Json"} name={name}/></td>
            <td className={"left"}><JSONConverter data={csvData} type={"JSON"} beautify={false} headers={headers} text={"Json-compact"} name={name}/></td>
            <td className={"left"}><JSONConverter data={csvData} type={"CSV"} headers={headers} separator={","} text={"CSV"} name={name}/></td>
          </tr>
          </tbody>
        </Table>
    }

    if(typeof this.state.apiKey !== "undefined" && typeof this.state.characters === "undefined"){return  <Loading/>}
    return <div style={{align:"center"}}>
      <Documentation url={"https://gitlab.com/Silvers_Gw2/Stats_Frontend/-/wikis/to_csv#inventory"} />
      <br/>
      {accountSelect}
      <br />
      <br />
      {characterSelect}
      <br />
      {firstSnapshot}
      <br />
      <div>{infoTable}</div>
      <br />
      {outputTable}
    </div>
  }
}

export default InventoryToCSV;
