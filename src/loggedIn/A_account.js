import React, {Component} from 'react';
import cookie from "react-cookies";
import {Redirect} from 'react-router-dom'
import { urlBase } from '../_utiilities/data.json'
import { Documentation} from '../_utiilities/functions_react'

function secondsToHms(d) {
    d = Number(d);
    let h = Math.floor(d / 3600);
    let m = Math.floor(d % 3600 / 60);
    let s = Math.floor(d % 3600 % 60);

    let hDisplay = h > 0 ? h + (m > 9 ? "h" : "h0") : "";
    let mDisplay = m > 0 ? m + (s > 9 ? "m" : "m0") : "";
    let sDisplay = s > 0 ? s + "s" : "";
    return hDisplay + mDisplay + sDisplay;
}

class A_account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            session: cookie.load('session'),
            error:false,
            errorCode:undefined,
            account: undefined,
            emailBox:"",
            passwordBox:"",
            apiBox:""
        };

        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePW = this.handleChangePW.bind(this);
        this.handleChangeAPI = this.handleChangeAPI.bind(this);
        this.handleSubmitEmail = this.handleSubmitEmail.bind(this);
        this.handleSubmitPassword = this.handleSubmitPassword.bind(this);
        this.handleSubmitAPI = this.handleSubmitAPI.bind(this);
        this.handleDeleteAPI = this.handleDeleteAPI.bind(this);
    }

    handleChangeEmail(event) {
        this.setState({emailBox: event.target.value});
    }
    handleChangePW(event) {
        this.setState({passwordBox: event.target.value});
    }
    handleChangeAPI(event) {
        this.setState({apiBox: event.target.value});
    }
    async loadData(url,extra) {return await fetch(url,extra).then(response => {return response.json()}).catch((err) => {console.log(this.props.url, err.toString()); return {result:"error", error:"could not fetch"};})}

    async loadAccount(tmp){
        let account = await this.loadData(urlBase.account + "/v1/website/account",{method: 'GET', headers: {'Content-Type': 'application/json', session: this.state.session}});
        if(account.result === "success"){
            tmp.account = account.account;
        }else{
            tmp.error = true;
            tmp.errorCode = account.error;
        }
        if(account.result ==="error"){
            if(account.error ==="Invalid session"){
                cookie.remove('session');
                tmp.session = undefined;
            }
        }

        return tmp;
    }

    async componentDidMount() {
        let tmp = {};
        tmp = await this.loadAccount(tmp);
        this.setState(tmp)
    }

    async handleSubmitEmail(event) {
        event.preventDefault();
        let tmp = {};
        let submitUrl = urlBase.account + "/v1/website/account/email";
        if(typeof this.state.emailBox !== "undefined"){
            let email = this.state.emailBox;
            await fetch(submitUrl, {method: 'POST', headers: {'Content-Type': 'application/json', session:this.state.session, email:email}})
                .then(async (response)=> {
                    let body = await response.json();
                    if (body.result === "success") {
                        tmp.emailBox = "";
                        tmp = await this.loadAccount(tmp);
                        alert("Success")
                    }
                    if (body.result === "error") {
                        alert(body.error)
                    }
                })
                .catch((err) => {alert(err)});
        }
        this.setState(tmp);
    }

    async handleSubmitPassword(event) {
        event.preventDefault();
        let tmp = {};
        let submitUrl = urlBase.account + "/v1/website/account/password";
        if(typeof this.state.passwordBox !== "undefined"){
            let password = this.state.passwordBox;
            await fetch(submitUrl, {method: 'POST', headers: {'Content-Type': 'application/json', session:this.state.session, password:password}})
                .then(async (response)=> {
                    let body = await response.json();
                    if (body.result === "success") {
                        tmp.passwordBox = "";
                        tmp = await this.loadAccount(tmp);
                        alert("Success")
                    }
                    if (body.result === "error") {
                        alert(body.error)
                    }
                })
                .catch((err) => {alert(err)});
        }
        this.setState(tmp);
    }

    async handleSubmitAPI(event) {
        event.preventDefault();
        let tmp = {};
        let submitUrl = urlBase.account + "/v1/website/account/apiKey";
        if(typeof this.state.apiBox !== "undefined"){
            let apiKey = this.state.apiBox;
            await fetch(submitUrl, {method: 'POST', headers: {'Content-Type': 'application/json', session:this.state.session, apiKey:apiKey}})
                .then(async (response)=> {
                    let body = await response.json();
                    if (body.result === "success") {
                        let pages = 120;
                        let buyFirstPage = await fetch("https://api.guildwars2.com/v2/commerce/transactions/history/buys?page_size=200&page=0&access_token=" + apiKey).then(res =>  {return res.headers.get("x-page-total")});
                        let sellFirstPage = await fetch("https://api.guildwars2.com/v2/commerce/transactions/history/sells?page_size=200&page=0&access_token=" + apiKey).then(res =>  {return res.headers.get("x-page-total")});

                        if(!isNaN(buyFirstPage)){
                            pages += (buyFirstPage-0)
                        }
                        if(!isNaN(sellFirstPage)){
                            pages += (sellFirstPage-0)
                        }

                        tmp.apiBox = "";
                        tmp = await this.loadAccount(tmp);
                        alert("Success, Processing may take up to: " + secondsToHms(pages));
                    }
                    if (body.result === "error") {
                        alert(body.error)
                    }
                })
                .catch((err) => {alert(err)});
        }
        this.setState(tmp);
    }

    async handleDeleteAPI(deleteKey) {
        let tmp = {};
        let submitUrl = urlBase.account + "/v1/website/account/deleteKey";
        await fetch(submitUrl, {method: 'POST', headers: {'Content-Type': 'application/json', session:this.state.session, apiKey:deleteKey}})
            .then(async (response)=> {
                let body = await response.json();
                if (body.result === "success") {
                    tmp = await this.loadAccount(tmp);
                    alert("Success")
                }
                if (body.result === "error") {
                    alert(body.error)
                }
            })
            .catch((err) => {alert(err)});

        this.setState(tmp);
    }


    render() {
        if(typeof this.state.session === "undefined"){
            return <Redirect to='/login'  />;
        }


        let info = <div>
            <p>
                This is where you manage your details
            </p>
        </div>;
        let error,details, user, apiSubmit, apiKeys;
        if(this.state.error === true){
            error =  <p>
                Error: {this.state.errorCode}
            </p>
        }
        if(this.state.account !== undefined){
            user = <div>
                <span>
                    <b>Username: </b>{this.state.account.user}.  <a href={"https://slate.silveress.ie/gw2_site#patreon"} target={"_blank"} rel={"noopener noreferrer"}><b>Account Level: </b>{this.state.account.accountLevel -1}</a>
                    <br />
                </span>
            </div>;
            let email = this.state.account.email;
            if(email === ""){
                email = "Email"
            }

            details = <div>
                <br />
                <h5>
                    Change your details:
                </h5>
                <form onSubmit={this.handleSubmitEmail}>
                    <input
                        type="text"
                        //required
                        value={this.state.emailBox}
                        onChange={this.handleChangeEmail}
                        placeholder={email}
                    />
                    &nbsp;
                    <input type="submit" value="Submit"/>
                </form>
                <br />
                <form onSubmit={this.handleSubmitPassword}>
                    <input
                        type="password"
                        //required
                        value={this.state.passwordBox}
                        onChange={this.handleChangePW}
                        placeholder={"New Password"}
                    />
                    &nbsp;
                    <input type="submit" value="Submit"/>
                </form>
                <br />
            </div>;

            apiSubmit = <div>
                API Keys
                <br />
                <form onSubmit={this.handleSubmitAPI}>
                    <div>
                        <input
                            type="text"
                            //required
                            value={this.state.apiBox}
                            onChange={this.handleChangeAPI}
                            placeholder={"API Key"}
                        />
                        &nbsp;
                        <input type="submit" value="Submit"/>
                    </div>
                </form>
                <br />
            </div>;

            let accounts = this.state.account.gameAccounts;
            let userIDs = Object.keys(accounts);

            if(userIDs.length > 0){
                let rows = [];
                for(let i=0;i<userIDs.length;i++){
                    let key = accounts[userIDs[i]].key;
                    rows.push(<tr key={key}>
                        <td className={"left"}>{userIDs[i]}</td>
                        <td className={"left"}>&nbsp;&nbsp;</td>
                        <td className={"left"} title={accounts[userIDs[i]].permissions.join("\n")} >{key}</td>
                        <td className={"left"}>&nbsp;&nbsp;</td>
                        <td className={"left"}><button onClick={async()=>{await this.handleDeleteAPI(key)}} style={{"background-color":"#ff5154", "border": "none"}}>Delete</button></td>
                    </tr>)
                }


                apiKeys = <div>
                    <table className={"centerTable table-primary table-striped table-highlight"} style={{ align:"center", margin:"0 auto" }} >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>&nbsp;&nbsp;</th>
                            <th>Key</th>
                            <th>&nbsp;&nbsp;</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        {rows}
                        </tbody>
                    </table>
                </div>;
            }

        }



        return <div>
            <Documentation url={"https://gitlab.com/Silvers_Gw2/Stats_Frontend/-/wikis/management#edit-details"} />
            <br/>
            {info}
            {error}
            {user}
            {details}
            {apiSubmit}
            {apiKeys}
        </div>
    }
}

export default A_account;